﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models
{
    public class ST_Menu_Opcion
    {
        [Key]
        public decimal Id_Menu_Opcion { get; set;}
        public string Descripcion { get; set; }
        public decimal Id_Menu_Padre { get; set; }
        public String Accion { get; set; }
        public string Controller { get; set; }
        public string Titulo { get; set; }
        public string Clase { get; set; }
    }
}
