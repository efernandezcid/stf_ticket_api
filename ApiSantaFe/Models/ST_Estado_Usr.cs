﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_Estado_Usr
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public string Cod_Estado_Usr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Dsc_Estado_Usr { get; set; }

    }
}
