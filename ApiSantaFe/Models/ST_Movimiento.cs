﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_Movimiento
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public decimal Id_Movimiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Fch_Movimiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Solicitud { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cod_Estado_Sol { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Usuario { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Observacion { get; set; }
        public int Horas_Dedicadas { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ST_Estado_Sol Estado_Movimiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ST_Usuario Usuario_Movimiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ST_Solicitud Solicitud_Movimiento { get; set; }

        public ST_Documento? Documento_Movimiento { get; set; }
    }
}
