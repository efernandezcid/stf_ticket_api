﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_Solicitud
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public decimal Id_Solicitud { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Fch_Ingreso { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Asunto_Sol { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Detalle_Sol { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Area { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Usuario { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Prioridad { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cod_Estado_Sol { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Fch_Estado_Sol { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Tipo_Sol { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ST_Area Area_Solicitud { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ST_Usuario Usuario_Solicitud { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ST_Prioridad Prioridad_Solicitud { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ST_Estado_Sol Estado_Solicitud { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ST_Tipo_Solicitud Tipo_Solicitud { get; set; }

    }
}
