﻿using ApiSantaFe.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models
{
    public class ST_Perfil_Usuario
    {
        [Key]
        public decimal Id_Perfil_Usuario { get; set; }
        public string Descripcion_Perfil { get; set; }
    }
            
}
