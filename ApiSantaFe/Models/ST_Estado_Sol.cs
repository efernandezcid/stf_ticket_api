﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models
{
    public class ST_Estado_Sol
    {
        [Key]
        public string Cod_Estado_Sol { get; set; }
        public string Dsc_Estado_Sol { get; set; }

    }
}
