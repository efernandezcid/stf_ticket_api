﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_Prioridad
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public decimal Id_Prioridad { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Dsc_Prioridad { get; set; }
    }
}
