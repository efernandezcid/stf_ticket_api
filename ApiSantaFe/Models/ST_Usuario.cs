﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_Usuario
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public decimal Id_Usuario { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Rut_Usuario { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string Dv_usuario { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string Nombre_Usr { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Ape_Usuario { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Fono_Usuario { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email_Usr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Fch_Insert { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Area { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Perfil_Usuario { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// 

        public string Cod_Estado_Usr { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public byte[] PasswordHash { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// 
        public byte[] PasswordSalt { get; set; }


        /// <summary>
        /// Area asociada al usuario
        /// </summary>
        public ST_Area Area_Usuario { get; set; }

        /// <summary>
        /// Estado asociado al usuario
        /// </summary>
        public ST_Estado_Usr Estado_Usuario { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ST_Perfil_Usuario Perfil_Usuario { get; set; }

    }
}
