﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    public class ST_Menu_OpcionDTO
    {
        [Key]
        public decimal Id_Menu_Opcion { get; set; }
        public string Descripcion { get; set; }
        public decimal Id_Menu_Padre { get; set; }
    }
}
