﻿using System;
using System.Collections.Generic;

namespace ApiSantaFe.Models.DTO
{
    public partial class ST_DocumentoDTO
    {
        [Key]
        public decimal IdDocumento { get; set; }
        public decimal IdMovimiento { get; set; }
        public string Extencion { get; set; }
        public string Evidencia { get; set; }
        public string Formato { get; set; }
        public string NombreDocumento { get; set; }
    }
}