﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_SolicitudDTO
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public decimal Id_Solicitud { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Fch_Ingreso { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Asunto_Sol { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Detalle_Sol { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Area { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Usuario { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Prioridad { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cod_Estado_Sol { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Fch_Estado_Sol { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Tipo_Sol { get; set; }
        
    }
}
