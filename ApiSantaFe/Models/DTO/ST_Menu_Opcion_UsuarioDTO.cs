﻿namespace ApiSantaFe.Models.DTO
{
    public class ST_Menu_Opcion_UsuarioDTO
    {
        public decimal Id_Menu_Opcion { get; set; }
        public decimal Id_Usuario { get; set; }
    }
}
