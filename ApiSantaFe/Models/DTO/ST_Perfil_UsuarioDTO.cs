﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    public class ST_Perfil_UsuarioDTO
    {
        public decimal Id_Perfil_Usuario { get; set; }
        public string Descripcion_Perfil { get; set; }
    }
}
