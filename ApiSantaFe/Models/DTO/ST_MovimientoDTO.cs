﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_MovimientoDTO
    {
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Movimiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Fch_Movimiento { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Solicitud { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Cod_Estado_Sol { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Usuario { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Observacion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Horas_Dedicadas  { get; set; }

    }
}
