﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    public class ST_Estado_SolDTO
    {
        [Key]
        public string Cod_Estado_Sol { get; set; }
        public string Dsc_Estado_Sol { get; set; }
    }
}
