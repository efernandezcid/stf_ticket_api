﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_Estado_UsrDTO
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public string Cod_Estado_Usr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Dsc_Estado_Usr { get; set; }
    }
}
