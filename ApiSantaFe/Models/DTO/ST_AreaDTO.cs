﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_AreaDTO
    {
        /// <summary>
        /// Este campo corresponde al Id del área que además es campo llave
        /// </summary>
        [Key]
        public decimal Id_Area { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Dsc_Area { get; set; }
    }
}
