﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    public class ST_Tipo_SolicitudDTO
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public decimal Id_Tipo_Sol { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Descripcion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Id_Area { get; set; }
    }
}
