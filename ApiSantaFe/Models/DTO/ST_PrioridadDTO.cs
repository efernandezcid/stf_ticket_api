﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    /// <summary>
    /// 
    /// </summary>
    public class ST_PrioridadDTO
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public decimal Id_Prioridad { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Dsc_Prioridad { get; set; }
    }
}
