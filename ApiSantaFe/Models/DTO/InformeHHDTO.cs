﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Models.DTO
{
    public class InformeHHDTO
    {
        [Key]
        public decimal Id_usuario { get; set; }
        public string Area_Descripcion { get; set; }
        public string Nombre_usuario { get; set; }
        public int Total_Horas { get; set; }
    }
}
