﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiSantaFe.Models
{
    public partial class ST_Documento
    {
        [Key]
        public decimal IdDocumento { get; set; }
        public decimal IdMovimiento { get; set; }
        public string Extencion { get; set; }
        public byte[] Documento { get; set; }
        public string Formato { get; set; }
        public string NombreDocumento { get; set; }
    }
}