﻿namespace ApiSantaFe.Models
{
    public class ST_Menu_Opcion_Usuario
    {
        public decimal Id_Menu_Opcion { get; set; }
        public decimal Id_Usuario { get; set; }
    }
}
