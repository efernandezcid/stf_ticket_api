using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository;
using ApiSantaFe.Repository.IRepository;
using ApiSantaFe.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(option =>
            option.UseSqlServer(Configuration.GetConnectionString("DefaultConectionstring")));
            services.AddScoped<IAreaRepository, AreaRepository>();
            services.AddScoped<IEstadoSolicitudRepository, EstadoSolicitudRepository>();
            services.AddScoped<IPrioridadRepository, PrioridadRepository>();
            services.AddScoped<IEstadoUsuarioRepository, EstadoUsuarioRepository>();
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
            services.AddScoped<ISolicitudRepository, SolicitudRepository>();
            services.AddScoped<IMovimientoRepository, MovimientoRepository>();
            services.AddScoped<ITipoSolicitudRepository, TipoSolicitudRepository>();
            services.AddScoped<ITipoSolicitudAreaRepository, TipoSolicitudAreaRepository>();
            services.AddScoped<IPerfilRepository, PerfilRepository>();
            services.AddScoped<IMenuOpcionRepository, MenuOpcionRepository>();
            services.AddScoped<IMenuOpcionUsuarioRepository, MenuOpcionUsuarioRepository>();

            services.AddScoped<IDocumentoRepository, DocumentoRepository>();
            services.AddScoped<IInformeHHRepository, InformeHHRepository>();
            services.AddScoped<IGraficoHHRepository, GraficoHHRepository>();
            //Agrega la estructura para los excel
            services.AddScoped<IExcelEscrituraService, ExcelEscrituraService>();

            services.AddAutoMapper(configuration =>
            {
                configuration.CreateMap<ST_Area, ST_AreaDTO>();
                configuration.CreateMap<ST_AreaDTO, ST_Area>();
                configuration.CreateMap<ST_Estado_Sol, ST_Estado_SolDTO>();
                configuration.CreateMap<ST_Estado_SolDTO, ST_Estado_Sol>();
                configuration.CreateMap<ST_Prioridad, ST_PrioridadDTO>();
                configuration.CreateMap<ST_PrioridadDTO, ST_Prioridad>();
                configuration.CreateMap<ST_Estado_Usr, ST_Estado_UsrDTO>();
                configuration.CreateMap<ST_Estado_UsrDTO, ST_Estado_Usr>();
                configuration.CreateMap<ST_Usuario, ST_UsuarioDTO>();
                configuration.CreateMap<ST_UsuarioDTO, ST_Usuario>();
                configuration.CreateMap<ST_Solicitud, ST_SolicitudDTO>();
                configuration.CreateMap<ST_SolicitudDTO, ST_Solicitud>();
                configuration.CreateMap<ST_Movimiento, ST_MovimientoDTO>();
                configuration.CreateMap<ST_MovimientoDTO, ST_Movimiento>();
                configuration.CreateMap<ST_Tipo_Solicitud, ST_Tipo_SolicitudDTO>();
                configuration.CreateMap<ST_Tipo_SolicitudDTO, ST_Tipo_Solicitud>();
                configuration.CreateMap<ST_Perfil_Usuario, ST_Perfil_UsuarioDTO>();
                configuration.CreateMap<ST_Perfil_UsuarioDTO, ST_Perfil_Usuario>();
                configuration.CreateMap<ST_Menu_Opcion, ST_Menu_OpcionDTO>();
                configuration.CreateMap<ST_Menu_OpcionDTO, ST_Menu_Opcion>();
                configuration.CreateMap<ST_Menu_Opcion_UsuarioDTO, ST_Menu_Opcion_Usuario>();

                configuration.CreateMap<ST_DocumentoDTO, ST_Documento>();
                configuration.CreateMap<ST_Documento, ST_DocumentoDTO>();

            }, typeof(Startup));

            services.AddControllers();
            AddSwagger(services);
        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupname = "v1";

                options.SwaggerDoc(groupname, new OpenApiInfo
                {
                    Title = $"API Grupo SantaFe,  {groupname}",
                    Version = groupname,
                    Description = "API Grupo SantaFe",
                    Contact = new OpenApiContact
                    {
                        Name = "Grupo SantaFe",
                        Email = string.Empty,
                        Url = new Uri("http://www.gruposantafe.cl/"),
                    }
                });
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Grupo SantaFe");
            });

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
