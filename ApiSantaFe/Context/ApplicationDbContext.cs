﻿using ApiSantaFe.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Context
{
    /// <summary>
    /// Contexto de la BD
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        /// <summary>
        /// SOBRE ESCRITURA DEL EVENTO DE CREACION DEL MODELO
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ST_Usuario>(entity =>
            {
                entity.HasKey(e => e.Id_Usuario)
                    .HasName("ST_USUARIO_PK");

                entity.ToTable("ST_USUARIO");

                entity.Property(e => e.Id_Usuario)
                    .HasColumnName("ID_USUARIO")
                    .HasColumnType("numeric(10,0)")
                    .ValueGeneratedOnAdd();
            });
            modelBuilder.Entity<ST_Usuario>()
           .HasOne(c => c.Area_Usuario)
           .WithMany().HasForeignKey(c => c.Id_Area);
            modelBuilder.Entity<ST_Usuario>()
           .HasOne(c => c.Estado_Usuario)
           .WithMany().HasForeignKey(c => c.Cod_Estado_Usr);
            modelBuilder.Entity<ST_Usuario>()
           .HasOne(c => c.Perfil_Usuario)
           .WithMany().HasForeignKey(c => c.Id_Perfil_Usuario);

            modelBuilder.Entity<ST_Solicitud>(entity =>
            {
                entity.HasKey(e => e.Id_Solicitud)
                    .HasName("ST_SOLICITUD_PK");

                entity.ToTable("ST_SOLICITUD");

                entity.Property(e => e.Id_Solicitud)
                    .HasColumnName("ID_SOLICITUD")
                    .HasColumnType("numeric(10,0)")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ST_Solicitud>()
           .HasOne(c => c.Area_Solicitud)
           .WithMany().HasForeignKey(c => c.Id_Area);
            modelBuilder.Entity<ST_Solicitud>()
           .HasOne(c => c.Usuario_Solicitud)
           .WithMany().HasForeignKey(c => c.Id_Usuario);
            modelBuilder.Entity<ST_Solicitud>()
           .HasOne(c => c.Prioridad_Solicitud)
           .WithMany().HasForeignKey(c => c.Id_Prioridad);
            modelBuilder.Entity<ST_Solicitud>()
           .HasOne(c => c.Estado_Solicitud)
           .WithMany().HasForeignKey(c => c.Cod_Estado_Sol);
            modelBuilder.Entity<ST_Solicitud>()
           .HasOne(c => c.Tipo_Solicitud)
           .WithMany().HasForeignKey(c => c.Id_Tipo_Sol);

            modelBuilder.Entity<ST_Movimiento>(entity =>
            {
                entity.HasKey(e => e.Id_Movimiento)
                    .HasName("ST_MOVIMIENTO_PK");

                entity.ToTable("ST_MOVIMIENTO");

                entity.Property(e => e.Id_Movimiento)
                    .HasColumnName("ID_MOVIMIENTO")
                    .HasColumnType("numeric(10,0)")
                    .ValueGeneratedOnAdd();
            });


            modelBuilder.Entity<ST_Movimiento>()
           .HasOne(c => c.Estado_Movimiento)
           .WithMany().HasForeignKey(c => c.Cod_Estado_Sol);
            modelBuilder.Entity<ST_Movimiento>()
           .HasOne(c => c.Usuario_Movimiento)
           .WithMany().HasForeignKey(c => c.Id_Usuario);
            modelBuilder.Entity<ST_Movimiento>()
           .HasOne(c => c.Solicitud_Movimiento)
           .WithMany().HasForeignKey(c => c.Id_Solicitud);
            modelBuilder.Entity<ST_Movimiento>()
           .HasOne(c => c.Documento_Movimiento)
           .WithMany().HasForeignKey(c => c.Id_Movimiento);

            modelBuilder.Entity<ST_Documento>(entity =>
            {
                entity.HasKey(e => e.IdDocumento)
                    .HasName("ST_DOCUMENTO_PK");

                entity.ToTable("ST_DOCUMENTO", "dbo");

                entity.Property(e => e.IdDocumento)
                    .HasColumnName("ID_DOCUMENTO")
                    .HasColumnType("numeric(10, 0)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Documento).HasColumnName("DOCUMENTO");

                entity.Property(e => e.Extencion)
                    .IsRequired()
                    .HasColumnName("EXTENCION")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.IdMovimiento)
                    .HasColumnName("ID_MOVIMIENTO")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Formato)
                    .IsRequired()
                    .HasColumnName("FORMATO")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NombreDocumento)
                    .IsRequired()
                    .HasColumnName("NOMBRE_DOCUMENTO")
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ST_Menu_Opcion_Usuario>(entity =>
            {
                entity.HasKey(e => new { e.Id_Menu_Opcion, e.Id_Usuario })
                    .HasName("ST_MENU_OPCION_USUARIO_PK");

                entity.ToTable("ST_MENU_OPCION_USUARIO", "dbo");

                entity.Property(e => e.Id_Menu_Opcion)
                    .HasColumnName("ID_MENU_OPCION")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Id_Usuario)
                    .HasColumnName("ID_USUARIO")
                    .HasColumnType("numeric(10, 0)");
            });

        }

        /// <summary>
        /// Constructor del contexto de la aplicación 
        /// </summary>
        /// <param name="options"></param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        /// <summary>
        /// Entidad que hace referencia a los clientes del grupo Santafe
        /// </summary>
        public DbSet<ST_Area> ST_Area { get; set; }

        /// <summary>
        /// Entidad que hace referencia a los estados de las solicitudes
        /// </summary>
        public DbSet<ST_Estado_Sol> ST_Estado_Sol { get; set; }

        /// <summary>
        /// Entidad que hace referencia a las prioridades de una solicitud
        /// </summary>
        public DbSet<ST_Prioridad> ST_Prioridad { get; set; }

        /// <summary>
        /// Entidad que hace referencia a los estados posibles para un usuario
        /// </summary>
        public DbSet<ST_Estado_Usr> ST_Estado_Usr { get; set; }

        /// <summary>
        /// Entidad que hace referencia a las solicitudes 
        /// </summary>
        public DbSet<ST_Solicitud> ST_Solicitud { get; set; }

        /// <summary>
        /// Entidad que hace referencia a los movimientos de las solicitudes (estados)
        /// </summary>
        public DbSet<ST_Movimiento> ST_Movimiento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ST_Tipo_Solicitud> ST_Tipo_Solicitud { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ST_Perfil_Usuario> ST_Perfil_Usuario { get; set; }

        /// <summary>
        /// Entidad que hace referencia a los usuarios 
        /// </summary>
        public DbSet<ST_Usuario> ST_Usuario { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ST_Documento> ST_Documento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<InformeHH> InformeHH { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<GraficoHH> GraficoHH { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ST_Menu_Opcion> ST_Menu_Opcion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ST_Menu_Opcion_Usuario> ST_Menu_Opcion_Usuario { get; set; }

    }
}
