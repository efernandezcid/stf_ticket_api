﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly ApplicationDbContext context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public UsuarioRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool CreateUsuario(ST_Usuario usuario, string password)
        {
            CrearPasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
            
            usuario.PasswordHash = passwordHash;
            usuario.PasswordSalt = passwordSalt;

            context.ST_Usuario.Add(usuario);

            return Save();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        public bool DeleteUsuario(ST_Usuario usuario)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ST_Usuario> GetAllUsuarios()
        {
            return context.ST_Usuario.Include(x => x.Area_Usuario).Include(x => x.Estado_Usuario).Include(x => x.Perfil_Usuario).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public ST_Usuario GetUsuario(decimal idUsuario)
        {
            return context.ST_Usuario.Include(x => x.Area_Usuario).Include(x => x.Estado_Usuario).Include(x => x.Perfil_Usuario).FirstOrDefault(x => x.Id_Usuario.Equals(idUsuario));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        public ST_Usuario GetUsuarioMail(string mail)
        {
            return context.ST_Usuario.Include(x => x.Area_Usuario).Include(x => x.Estado_Usuario).FirstOrDefault(x => x.Email_Usr.ToUpper().Equals(mail.ToUpper()));
        }

        /// <summary>
        /// Metodo que permite autenticar un usuario en el sistema
        /// </summary>
        /// <param name="mail"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public EstatusLogin GetLoginUsuario(string mail, string password)
        {
            EstatusLogin resultado = new EstatusLogin();

            var user = context.ST_Usuario.Include(x => x.Area_Usuario).Include(x => x.Estado_Usuario).FirstOrDefault(x => x.Email_Usr.ToUpper().Equals(mail.ToUpper()));
            if (user == null)
            {
                resultado.Codigo = "er_usr";
                resultado.Descripcion = "Usuario Incorrecto";
                return resultado;
            }
            else if (!VerificarPasswordHash(password, user.PasswordHash, user.PasswordSalt)) 
            {
                resultado.Codigo = "er_psw";
                resultado.Descripcion = "Password Incorrecto";
                return resultado;
            }
            else if ((user.Estado_Usuario.Cod_Estado_Usr.ToUpper() == "ANU"))
            {
                resultado.Codigo = "er_est";
                resultado.Descripcion = "El Usario se encuentra Anulado";
                return resultado;
            }
            else
            {
                resultado.Codigo = "ok";
                resultado.Descripcion = "Usuario Validado";
                return resultado;
            }
            
        }

        /// <summary>
        /// Métodp que devuelve listado de usuarios por un área definida
        /// </summary>
        /// <param name="id_Area"></param>
        /// <returns></returns>
        public IEnumerable<ST_Usuario> GetUsuarioArea(decimal id_Area)
        {
            return context.ST_Usuario.Include(x => x.Area_Usuario).Include(x => x.Estado_Usuario).Where(x => x.Id_Area.Equals(id_Area)).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool UsuarioExists(string email) 
        {
            return context.ST_Usuario.Any(x => x.Email_Usr.ToLower().Trim().Equals(email.ToLower().Trim()));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            return context.SaveChanges() > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool UpdateUsuario(ST_Usuario usuario, string password)
        {
            var objUsuario = context.ST_Usuario.Find(usuario.Id_Usuario);

            if (password == null)
            {
                objUsuario.Rut_Usuario = usuario.Rut_Usuario;
                objUsuario.Dv_usuario = usuario.Dv_usuario;
                objUsuario.Nombre_Usr = usuario.Nombre_Usr;
                objUsuario.Ape_Usuario = usuario.Ape_Usuario;
                objUsuario.Fono_Usuario = usuario.Fono_Usuario;
                objUsuario.Email_Usr = usuario.Email_Usr;
                objUsuario.Id_Area = usuario.Id_Area;
                objUsuario.Id_Perfil_Usuario = usuario.Id_Perfil_Usuario;
                objUsuario.Cod_Estado_Usr = usuario.Cod_Estado_Usr;
            }
            else
            {
                CrearPasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
                objUsuario.Cod_Estado_Usr = usuario.Cod_Estado_Usr;
                objUsuario.PasswordHash = passwordHash;
                objUsuario.PasswordSalt = passwordSalt;
            }

            return Save();
        }


        private void CrearPasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }

        }

        private bool VerificarPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                        return false;
                }
                return true;
            }
        }

    }
}
