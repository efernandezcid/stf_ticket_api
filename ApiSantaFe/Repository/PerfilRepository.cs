﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class PerfilRepository: IPerfilRepository
    {
        private readonly ApplicationDbContext context;

        public PerfilRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool CreatePerfilUsuario(ST_Perfil_Usuario perfilUsuario)
        {
            throw new NotImplementedException();
        }

        public bool DeletePerfilUsuario(ST_Perfil_Usuario perfilUsuario)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ST_Perfil_Usuario> GetAllPerfilesUsuario()
        {
            return context.ST_Perfil_Usuario.ToList();
        }

        public ST_Perfil_Usuario GetPerfilUsuario(int id)
        {
            throw new NotImplementedException();
        }

        public bool PerfilUsuarioExists(string descripcion)
        {
            throw new NotImplementedException();
        }

        public bool PerfilUsuarioExists(int id)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public bool UpdatePerfilUsuario(ST_Perfil_Usuario perfilUsuario)
        {
            throw new NotImplementedException();
        }
    }
}
