﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public class MovimientoRepository : IMovimientoRepository
    {
        private readonly ApplicationDbContext context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public MovimientoRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="movimiento"></param>
        /// <returns></returns>
        public bool CreateMovimiento(ST_Movimiento movimiento)
        {
            context.ST_Movimiento.Add(movimiento);
            return Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="movimiento"></param>
        /// <returns></returns>
        public bool DeleteMovimiento(ST_Movimiento movimiento)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ST_Movimiento> GetAllMovimientos()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idMovimiento"></param>
        /// <returns></returns>
        public ST_Movimiento GetMovimiento(decimal idMovimiento)
        {
            return context.ST_Movimiento.Include(x => x.Usuario_Movimiento).Include(x => x.Estado_Movimiento).FirstOrDefault(x => x.Id_Movimiento.Equals(idMovimiento));
        }

        /// <summary>
        /// Rescata los movimientos para devolver bandeja para el usuario (solicitudes pendientes de trabajo)
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public IEnumerable<ST_Movimiento> GetMovimientosBandejaUsuario(decimal idUsuario)
        {
            return context.ST_Movimiento.Include(x => x.Usuario_Movimiento).Include(x => x.Estado_Movimiento).Include(x => x.Solicitud_Movimiento).Include(x => x.Solicitud_Movimiento.Usuario_Solicitud).Include(x => x.Solicitud_Movimiento.Prioridad_Solicitud).Where(x => x.Fch_Movimiento.Equals(x.Solicitud_Movimiento.Fch_Estado_Sol) && x.Id_Usuario.Equals(idUsuario) && ( x.Cod_Estado_Sol.Equals("ASI") || x.Cod_Estado_Sol.Equals("PRC") || x.Cod_Estado_Sol.Equals("DEV"))).ToList();
        }

        /// <summary>
        /// Rescata los movimientos para devolver bandeja para el usuario (solicitudes pendientes de trabajo)
        /// </summary>
        /// <param name="idSolicitud"></param>
        /// <returns></returns>
        public IEnumerable<ST_Movimiento> GetMovimientosSolicitud(decimal idSolicitud)
        {
            return context.ST_Movimiento.Include(x => x.Usuario_Movimiento).Include(x => x.Estado_Movimiento).Where(x => x.Id_Solicitud.Equals(idSolicitud)).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            return context.SaveChanges() > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="movimiento"></param>
        /// <returns></returns>
        public bool UpdateMovimiento(ST_Movimiento movimiento)
        {
            throw new NotImplementedException();
        }
    }
}
