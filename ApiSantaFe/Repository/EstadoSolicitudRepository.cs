﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class EstadoSolicitudRepository : IEstadoSolicitudRepository
    {
        private readonly ApplicationDbContext context;

        public EstadoSolicitudRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool CreateEstadoSolicitud(ST_Estado_Sol estadoSolicitud)
        {
            throw new NotImplementedException();
        }

        public bool DeleteEstadoSolicitud(ST_Estado_Sol estadoSolicitud)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ST_Estado_Sol> GetAllEstadosSolicitud()
        {
            return context.ST_Estado_Sol.ToList();
        }

        public ST_Estado_Sol GetEstadoSolicitud(string codEstado)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public bool UpdateEstadoSolicitud(ST_Estado_Sol estadoSolicitud)
        {
            throw new NotImplementedException();
        }
    }
}
