﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class GraficoHHRepository : IGraficoHHRepository
    {
        private readonly ApplicationDbContext context;

        public GraficoHHRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<GraficoHH> GetDatosGrafico(int idArea, int mes, int ano)
        {
            var datosGrafico = context
              .GraficoHH
              .FromSqlInterpolated(
                 $"EXEC SP_OBTIENE_INFO_HH_GRAFICO @ID_AREA = {idArea}, @MES = {mes}, @ANO = {ano}")
              .ToList();

            return datosGrafico;
        }
    }
}
