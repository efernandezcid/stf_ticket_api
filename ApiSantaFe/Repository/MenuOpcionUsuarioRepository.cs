﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
namespace ApiSantaFe.Repository
{
    public class MenuOpcionUsuarioRepository : IMenuOpcionUsuarioRepository
    {
        private readonly ApplicationDbContext context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public MenuOpcionUsuarioRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool DeleteMenuOpcionUsuario(ST_Menu_Opcion_Usuario menuOpcionUsuario)
        {
            context.ST_Menu_Opcion_Usuario.Remove(menuOpcionUsuario);
            return Save();
        }
        public bool CreateMenuOpcionUsuario(ST_Menu_Opcion_Usuario menuOpcionUsuario)
        {
            context.ST_Menu_Opcion_Usuario.Add(menuOpcionUsuario);
            return Save();
            
        }

        public bool Save()
        {
            return context.SaveChanges() > 0;
        }

    }
}
    