﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class AreaRepository : IAreaRepository
    {
        private readonly ApplicationDbContext context;

        public AreaRepository(ApplicationDbContext context)
        {
            this.context = context;
        }


        public bool CreateArea(ST_Area area)
        {
            throw new NotImplementedException();
        }

        public bool DeleteArea(ST_Area area)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ST_Area> GetAllAreas()
        {
            return context.ST_Area.ToList();
        }

        public ST_Area GetArea(decimal idArea)
        {
            return context.ST_Area.FirstOrDefault(x => x.Id_Area.Equals(idArea));
        }

        public bool Save()
        {
            return context.SaveChanges() > 0;
        }

        public bool UpdateArea(ST_Area area)
        {
            context.ST_Area.Update(area);
            return Save();
        }
    }
}
