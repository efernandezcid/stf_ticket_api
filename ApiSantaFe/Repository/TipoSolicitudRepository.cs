﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public class TipoSolicitudRepository : ITipoSolicitudRepository
    {
        private readonly ApplicationDbContext context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public TipoSolicitudRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoSolicitud"></param>
        /// <returns></returns>
        public bool CreateTipoSolicitud(ST_Tipo_Solicitud tipoSolicitud)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoSolicitud"></param>
        /// <returns></returns>
        public bool DeleteTipoSolicitud(ST_Tipo_Solicitud tipoSolicitud)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ST_Tipo_Solicitud> GetAllTiposSolicitud()
        {
            return context.ST_Tipo_Solicitud.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTipoSolicitud"></param>
        /// <returns></returns>
        public ST_Tipo_Solicitud GetTipoSolicitud(decimal idTipoSolicitud)
        {
            return context.ST_Tipo_Solicitud.FirstOrDefault(x => x.Id_Tipo_Sol.Equals(idTipoSolicitud));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipoSolicitud"></param>
        /// <returns></returns>
        public bool UpdateTipoSolicitud(ST_Tipo_Solicitud tipoSolicitud)
        {
            throw new NotImplementedException();
        }
    }
}
