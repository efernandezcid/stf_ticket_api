﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class MenuOpcionRepository : IMenuOpcionRepository
    {
        private readonly ApplicationDbContext context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public MenuOpcionRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool CreateMenuOpcion(ST_Menu_Opcion menuOpcion)
        {
            throw new NotImplementedException();
        }

        public bool DeleteMenuOpcion(ST_Menu_Opcion menuOpcion)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ST_Menu_Opcion> GetAllMenuOpcion()
        {
            return context.ST_Menu_Opcion.ToList();
        }

        public IEnumerable<ST_Menu_Opcion> GetMenuOpcionDependencia(decimal idPadre)
        {
            return context.ST_Menu_Opcion.Where(x => x.Id_Menu_Padre.Equals(idPadre)).ToList();
        }

        public IEnumerable<ST_Menu_Opcion> GetMenuSinAccesoXusuario(decimal idMenuPadre, decimal idusuario)
        {
            var datosMenu = context
            .ST_Menu_Opcion
            .FromSqlInterpolated(
               $"EXEC [SP_OBTIENE_PAGINAS_SIN_ACCESO] @ID_MENU_PADRE = {idMenuPadre}, @ID_USUARIO = {idusuario}")
            .ToList();
            return datosMenu;
        }

        public IEnumerable<ST_Menu_Opcion> GetMenuConAccesoXusuario(decimal idMenuPadre, decimal idusuario)
        {
            var datosMenu = context
            .ST_Menu_Opcion
            .FromSqlInterpolated(
               $"EXEC [SP_OBTIENE_PAGINAS_CON_ACCESO] @ID_MENU_PADRE = {idMenuPadre}, @ID_USUARIO = {idusuario}")
            .ToList();
            return datosMenu;
        }

        public Task<ST_Menu_Opcion> GetMenuOpcion(decimal id)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public bool UpdateMenuOpcion(ST_Menu_Opcion menuOpcion)
        {
            throw new NotImplementedException();
        }
    }
}
