﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class InformeHHRepository : IInformeHHRepository
    {
        private readonly ApplicationDbContext context;

        public InformeHHRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<InformeHH> GetInforme(int idArea, int mes, int ano)
        {
            var datosInforme = context
               .InformeHH
               .FromSqlInterpolated(
                  $"EXEC SP_OBTIENE_REPORTE_HH_X_AREA @ID_AREA = {idArea}, @MES = {mes}, @ANO = {ano}")
               .ToList();

            return datosInforme;
        }


    }
}
