﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace ApiSantaFe.Repository
{
    /// <summary>
    /// 
    /// </summary>
    public class SolicitudRepository : ISolicitudRepository
    {
        private readonly ApplicationDbContext context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public SolicitudRepository(ApplicationDbContext context)
        {
            this.context = context;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="solicitud"></param>
        /// <param name="evidenciaSol"></param>
        /// <returns></returns>
        public bool CreateSolicitud(ST_Solicitud solicitud)
        {
            context.ST_Solicitud.Add(solicitud);
            return Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="solicitud"></param>
        /// <returns></returns>
        public bool DeleteSolicitud(ST_Solicitud solicitud)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ST_Solicitud> GetAllSolicitudes()
        {
            return context.ST_Solicitud.Include(x => x.Area_Solicitud).Include(x => x.Usuario_Solicitud).Include(x => x.Prioridad_Solicitud).Include(x => x.Estado_Solicitud).Include(x => x.Tipo_Solicitud).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idSolicitud"></param>
        /// <returns></returns>
        public ST_Solicitud GetSolicitud(decimal idSolicitud)
        {
            return context.ST_Solicitud.Include(x => x.Area_Solicitud).Include(x => x.Usuario_Solicitud).Include(x => x.Prioridad_Solicitud).Include(x => x.Estado_Solicitud).Include(x => x.Tipo_Solicitud).FirstOrDefault(x => x.Id_Solicitud.Equals(idSolicitud));
        }


        /// <summary>
        /// Devuelve las solicitudes en estado de ingreso por área (bandeja para tomar solicitudes)
        /// </summary>
        /// <param name="idArea"></param>
        /// <returns></returns>
        public IEnumerable<ST_Solicitud> GetSolicitudAreaIngresada(decimal idArea)
        {
            return context.ST_Solicitud.Include(x => x.Area_Solicitud).Include(x => x.Usuario_Solicitud).Include(x => x.Prioridad_Solicitud).Include(x => x.Tipo_Solicitud).Include(x => x.Estado_Solicitud).Where(x => x.Id_Area.Equals(idArea) && x.Cod_Estado_Sol.Equals("ING")).ToList();
        }

        /// <summary>
        /// Devuelve las solicitudes ingresadas por un usuario (bandeja seguimiento)
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public IEnumerable<ST_Solicitud> GetSolicitudUsuarioIngresada(decimal idUsuario)
        {
            List<ST_Solicitud> solicitudes = context.ST_Solicitud.Include(x => x.Area_Solicitud).Include(x => x.Usuario_Solicitud).Include(x => x.Prioridad_Solicitud).Include(x => x.Tipo_Solicitud).Include(x => x.Estado_Solicitud).Where(x => x.Id_Usuario.Equals(idUsuario) && !x.Cod_Estado_Sol.Equals("CER")).ToList();
            return solicitudes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            return context.SaveChanges() > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="solicitud"></param>
        /// <returns></returns>
        public bool UpdateSolicitud(ST_Solicitud solicitud)
        {
            context.ST_Solicitud.Update(solicitud);
            return Save();
        }
    }
}
