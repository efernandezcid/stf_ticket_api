﻿using ApiSantaFe.Models;
using ApiSantaFe.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IUsuarioRepository
    {
        IEnumerable<ST_Usuario> GetAllUsuarios();

        ST_Usuario GetUsuario(decimal idUsuario);

        ST_Usuario GetUsuarioMail(string mail);

        EstatusLogin GetLoginUsuario(string mail, string password);

        bool UsuarioExists(string email);

        IEnumerable<ST_Usuario> GetUsuarioArea(decimal id_Area);

        bool CreateUsuario(ST_Usuario usuario, string pasword);

        bool DeleteUsuario(ST_Usuario usuario);

        bool UpdateUsuario(ST_Usuario usuario, string password);

        bool Save();

    }
}
