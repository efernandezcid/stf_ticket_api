﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IMenuOpcionRepository
    {
        IEnumerable<ST_Menu_Opcion> GetAllMenuOpcion();

        IEnumerable<ST_Menu_Opcion> GetMenuOpcionDependencia(decimal idPadre);

        IEnumerable<ST_Menu_Opcion> GetMenuSinAccesoXusuario(decimal idMenuPadre, decimal idusuario);

        IEnumerable<ST_Menu_Opcion> GetMenuConAccesoXusuario(decimal idMenuPadre, decimal idusuario);

        Task<ST_Menu_Opcion> GetMenuOpcion(decimal id);

        bool CreateMenuOpcion(ST_Menu_Opcion menuOpcion);

        bool UpdateMenuOpcion(ST_Menu_Opcion menuOpcion);

        bool DeleteMenuOpcion(ST_Menu_Opcion menuOpcion);

        bool Save();
    }
}
