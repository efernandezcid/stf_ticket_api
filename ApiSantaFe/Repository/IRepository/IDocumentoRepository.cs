﻿using ApiSantaFe.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IDocumentoRepository
    {
        IEnumerable<ST_Documento> GetAllDocumento();

        Task<ST_Documento> GetDocumento(decimal idDocumento);

        ST_Documento GetDocumentoXMovimiento(decimal idMovimiento);

        bool CreateDocumento(ST_Documento documento, string evidencia);

        bool UpdateDocumento(ST_Documento documento);

        bool DeleteDocumento(ST_Documento documento);

        bool Save();
    }
}
