﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IMenuOpcionUsuarioRepository
    {
        bool CreateMenuOpcionUsuario(ST_Menu_Opcion_Usuario menuOpcion);

        bool DeleteMenuOpcionUsuario(ST_Menu_Opcion_Usuario menuOpcion);

        bool Save();
    }
}
