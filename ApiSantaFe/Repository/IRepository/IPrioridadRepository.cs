﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IPrioridadRepository
    {
        IEnumerable<ST_Prioridad> GetAllPrioridades();

        ST_Prioridad GetPrioridad(decimal idPrioridad);

        bool CreatePrioridad(ST_Prioridad prioridad);

        bool UpdatePrioridad(ST_Prioridad prioridad);

        bool DeletePrioridad(ST_Prioridad prioridad);

        bool Save();
    }
}
