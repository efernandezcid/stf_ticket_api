﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IAreaRepository
    {
        IEnumerable<ST_Area> GetAllAreas();

        ST_Area GetArea(decimal idArea);

        bool CreateArea(ST_Area area);

        bool UpdateArea(ST_Area area);

        bool DeleteArea(ST_Area area);

        bool Save();
    }
}
