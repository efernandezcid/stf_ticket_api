﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IPerfilRepository
    {
        IEnumerable<ST_Perfil_Usuario> GetAllPerfilesUsuario();

        ST_Perfil_Usuario GetPerfilUsuario(int id);

        bool PerfilUsuarioExists(string descripcion);

        bool PerfilUsuarioExists(int id);

        bool CreatePerfilUsuario(ST_Perfil_Usuario perfilUsuario);

        bool UpdatePerfilUsuario(ST_Perfil_Usuario perfilUsuario);

        bool DeletePerfilUsuario(ST_Perfil_Usuario perfilUsuario);

        bool Save();
    }
}
