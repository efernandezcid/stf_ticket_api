﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface ISolicitudRepository
    {
        IEnumerable<ST_Solicitud> GetAllSolicitudes();

        ST_Solicitud GetSolicitud(decimal idSolicitud);

        IEnumerable<ST_Solicitud> GetSolicitudAreaIngresada(decimal idArea);

        IEnumerable<ST_Solicitud> GetSolicitudUsuarioIngresada(decimal idUsuario);

        bool CreateSolicitud(ST_Solicitud solicitud);

        bool UpdateSolicitud(ST_Solicitud solicitud);

        bool DeleteSolicitud(ST_Solicitud solicitud);

        bool Save();
    }
}
