﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IGraficoHHRepository
    {
        IEnumerable<GraficoHH> GetDatosGrafico(int idArea, int mes, int ano);
    }
}
