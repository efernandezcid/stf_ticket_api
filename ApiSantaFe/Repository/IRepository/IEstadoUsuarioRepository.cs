﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface IEstadoUsuarioRepository
    {
        IEnumerable<ST_Estado_Usr> GetAllEstadosUsuario();

        ST_Estado_Usr GetEstadoUsuario(string codEstado);

        bool CreateEstadoUsuario(ST_Estado_Usr estadoUsuario);

        bool UpdateEstadoUsuario(ST_Estado_Usr estadoUsuario);

        bool DeleteEstadoUsuario(ST_Estado_Usr estadoUsuario);

        bool Save();
    }
}
