﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface ITipoSolicitudRepository
    {
        IEnumerable<ST_Tipo_Solicitud> GetAllTiposSolicitud();

        ST_Tipo_Solicitud GetTipoSolicitud(decimal idTipoSolicitud);

        bool CreateTipoSolicitud(ST_Tipo_Solicitud tipoSolicitud);

        bool UpdateTipoSolicitud(ST_Tipo_Solicitud tipoSolicitud);

        bool DeleteTipoSolicitud(ST_Tipo_Solicitud tipoSolicitud);

        bool Save();
    }
}
