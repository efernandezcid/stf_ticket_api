﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    /// <summary>
    /// 
    /// </summary>
    public interface IMovimientoRepository
    {
        IEnumerable<ST_Movimiento> GetAllMovimientos();

        ST_Movimiento GetMovimiento(decimal idMovimiento);

        IEnumerable<ST_Movimiento> GetMovimientosBandejaUsuario(decimal idUsuario);

        IEnumerable<ST_Movimiento> GetMovimientosSolicitud(decimal idSolicitud);

        bool CreateMovimiento(ST_Movimiento movimiento);

        bool UpdateMovimiento(ST_Movimiento movimiento);

        bool DeleteMovimiento(ST_Movimiento movimiento);

        bool Save();
    }
}
