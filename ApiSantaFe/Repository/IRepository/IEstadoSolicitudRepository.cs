﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public interface IEstadoSolicitudRepository
    {
        IEnumerable<ST_Estado_Sol> GetAllEstadosSolicitud();

        ST_Estado_Sol GetEstadoSolicitud(string codEstado);

        bool CreateEstadoSolicitud(ST_Estado_Sol estadoSolicitud);

        bool UpdateEstadoSolicitud(ST_Estado_Sol estadoSolicitud);

        bool DeleteEstadoSolicitud(ST_Estado_Sol estadoSolicitud);

        bool Save();
    }
}
