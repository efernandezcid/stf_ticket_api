﻿using ApiSantaFe.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository.IRepository
{
    public interface ITipoSolicitudAreaRepository
    {
        IEnumerable<ST_Tipo_Solicitud> GetAllTiposSolicitudArea(decimal IdArea);
    }
}
