﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class EstadoUsuarioRepository : IEstadoUsuarioRepository
    {
        private readonly ApplicationDbContext context;

        public EstadoUsuarioRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool CreateEstadoUsuario(ST_Estado_Usr estadoUsuario)
        {
            throw new NotImplementedException();
        }

        public bool DeleteEstadoUsuario(ST_Estado_Usr estadoUsuario)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ST_Estado_Usr> GetAllEstadosUsuario()
        {
            return context.ST_Estado_Usr.ToList();
        }

        public ST_Estado_Usr GetEstadoUsuario(string codEstado)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public bool UpdateEstadoUsuario(ST_Estado_Usr estadoUsuario)
        {
            throw new NotImplementedException();
        }
    }
}
