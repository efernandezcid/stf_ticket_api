﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class TipoSolicitudAreaRepository : ITipoSolicitudAreaRepository
    {
        private readonly ApplicationDbContext context;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public TipoSolicitudAreaRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<ST_Tipo_Solicitud> GetAllTiposSolicitudArea(decimal idArea)
        {
            return context.ST_Tipo_Solicitud.Where(x => x.Id_Area.Equals(idArea)).ToList();
        }
    }
}
