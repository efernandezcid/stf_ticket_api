﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class DocumentoRepository : IDocumentoRepository
    {
        private readonly ApplicationDbContext context;

        public DocumentoRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool DeleteDocumento(ST_Documento documento)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<ST_Documento> GetAllDocumento()
        {
            throw new System.NotImplementedException();
        }

        public async Task<ST_Documento> GetDocumento(decimal idDocumento)
        {
            return await context.ST_Documento.FindAsync(idDocumento);
        }

        public ST_Documento GetDocumentoXMovimiento(decimal idMovimiento)
        {
            return  context.ST_Documento.Where(x => x.IdMovimiento.Equals(idMovimiento)).FirstOrDefault();
        }


        public bool UpdateDocumento(ST_Documento documento)
        {
            throw new System.NotImplementedException();
        }

        public bool CreateDocumento(ST_Documento documento, string evidencia)
        {
            documento.Documento = Convert.FromBase64String(evidencia);
            context.ST_Documento.Add(documento);
            return Save();
        }
        
        public bool Save()
        {
            return context.SaveChanges() > 0;
        }
    }
}
