﻿using ApiSantaFe.Context;
using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Repository
{
    public class PrioridadRepository : IPrioridadRepository
    {
        private readonly ApplicationDbContext context;

        public PrioridadRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public bool CreatePrioridad(ST_Prioridad prioridad)
        {
            throw new NotImplementedException();
        }

        public bool DeletePrioridad(ST_Prioridad prioridad)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ST_Prioridad> GetAllPrioridades()
        {
            return context.ST_Prioridad.ToList();
        }

        public ST_Prioridad GetPrioridad(decimal idPrioridad)
        {
            throw new NotImplementedException();
        }

        public bool Save()
        {
            throw new NotImplementedException();
        }

        public bool UpdatePrioridad(ST_Prioridad prioridad)
        {
            throw new NotImplementedException();
        }
    }
}
