﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiSantaFe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuOpcionUsuarioController : Controller
    {
        private readonly IMenuOpcionUsuarioRepository menuOpcionUsuarioRepository;
        private readonly IMapper mapper;

        public MenuOpcionUsuarioController(IMenuOpcionUsuarioRepository menuOpcionUsuarioRepository, IMapper mapper)
        {
            this.menuOpcionUsuarioRepository = menuOpcionUsuarioRepository;
            this.mapper = mapper;
        }

        [HttpPost]
        public IActionResult CreateMenuOpcionUsuario([FromBody] ST_Menu_Opcion_Usuario menuOpcionUsuarioDTO)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var relacionMenuUsuario = mapper.Map<ST_Menu_Opcion_Usuario>(menuOpcionUsuarioDTO);

            if (!menuOpcionUsuarioRepository.CreateMenuOpcionUsuario(relacionMenuUsuario))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar registrar la relación del menu");
                return StatusCode(500, ModelState);
            }

            return Ok();    
        }

        [HttpPut]
        public IActionResult DeleteMenuOpcionUsuario([FromBody] ST_Menu_Opcion_Usuario menuOpcionUsuarioDTO)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var relacionMenuUsuario = mapper.Map<ST_Menu_Opcion_Usuario>(menuOpcionUsuarioDTO);

            if (!menuOpcionUsuarioRepository.DeleteMenuOpcionUsuario(relacionMenuUsuario))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar eliminar la relación del menu");
                return StatusCode(500, ModelState);
            }

            return Ok();
        }

    }
}
