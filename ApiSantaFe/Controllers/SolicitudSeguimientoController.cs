﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de solicitudes ingresadas por un usuario
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitudSeguimientoController : Controller
    {
        private readonly ISolicitudRepository solicitudRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de solicitudes ingresadas por un usuario
        /// </summary>
        /// <param name="solicitudRepository"></param>
        /// <param name="mapper"></param>
        public SolicitudSeguimientoController(ISolicitudRepository solicitudRepository, IMapper mapper)
        {
            this.solicitudRepository = solicitudRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve las solicitudes que ha generado un usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idUsuario}", Name = "GetSolicitudesIngUsuario")]
        public IActionResult GetSolicitudesIngUsuario(decimal idUsuario)
        {
            var solicitudes = solicitudRepository.GetSolicitudUsuarioIngresada(idUsuario);
            if (solicitudes == null) return NotFound();
            return Ok(mapper.Map<List<ST_Solicitud>>(solicitudes));
        }
    }
}
