﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de movimientos por solicitud
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MovimientoSolicitudController : Controller
    {
        private readonly IMovimientoRepository movimientoRepository;
        private readonly IDocumentoRepository documentoRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de solicitudes ingresadas por un usuario
        /// </summary>
        /// <param name="movimientoRepository"></param>
        /// <param name="mapper"></param>
        public MovimientoSolicitudController(IMovimientoRepository movimientoRepository, IDocumentoRepository documentoRepository, IMapper mapper)
        {
            this.movimientoRepository = movimientoRepository;
            this.documentoRepository = documentoRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve los movimientos de una solicitud
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idSolicitud}", Name = "GetMovimientosSolicitud")]
        public IActionResult GetMovimientosSolicitud(decimal idSolicitud)
        {
      
            var movimientos = movimientoRepository.GetMovimientosSolicitud(idSolicitud);
            if (movimientos == null) return NotFound();

            foreach(var mov in movimientos)
            {
                var documento = documentoRepository.GetDocumentoXMovimiento(mov.Id_Movimiento);
                if(!(documento == null))
                {
                    documento.Documento = null;
                    mov.Documento_Movimiento = documento;
                }
            }

            return Ok(mapper.Map<List<ST_Movimiento>>(movimientos));
        }
    }
}
