﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de solicitudes ingresadas por area
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitudIngArea : Controller
    {
        private readonly ISolicitudRepository solicitudRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de usuarios
        /// </summary>
        /// <param name="solicitudRepository"></param>
        /// <param name="mapper"></param>
        public SolicitudIngArea(ISolicitudRepository solicitudRepository, IMapper mapper)
        {
            this.solicitudRepository = solicitudRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve las solicitudes en estado ingresada por area
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idArea}", Name = "GetSolicitudesIngArea")]
        public IActionResult GetSolicitudesIngArea(decimal idArea)
        {
            var solicitudes = solicitudRepository.GetSolicitudAreaIngresada(idArea);
            if (solicitudes == null) return NotFound();
            return Ok(mapper.Map<List<ST_Solicitud>>(solicitudes));
        }
    }
}
