﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MenuConAccesoController : Controller
    {
        private readonly IMenuOpcionRepository menuOpcionRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuOpcionRepository"></param>
        /// <param name="mapper"></param>
        public MenuConAccesoController(IMenuOpcionRepository menuOpcionRepository, IMapper mapper)
        {
            this.menuOpcionRepository = menuOpcionRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve los menu con acceso por usuario
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idPadre}/{idusuario}", Name = "GetAllMenuConAccesoXusuario")]
        public IActionResult GetAllMenuConAccesoXusuario(decimal idPadre, decimal idusuario)
        {
            var menuOpciones = menuOpcionRepository.GetMenuConAccesoXusuario(idPadre, idusuario);
            return Ok(mapper.Map<List<ST_Menu_Opcion>>(menuOpciones));
        }
    }
}
