﻿using ApiSantaFe.Models;
using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de solicitudes
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SolicitudController : Controller
    {
        private readonly ISolicitudRepository solicitudRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de usuarios
        /// </summary>
        /// <param name="solicitudRepository"></param>
        /// <param name="mapper"></param>
        public SolicitudController(ISolicitudRepository solicitudRepository, IMapper mapper)
        {
            this.solicitudRepository = solicitudRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve todas las solicitudes de la base de datos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllSolicitudes()
        {
            var solicitudes = solicitudRepository.GetAllSolicitudes();
            return Ok(mapper.Map<List<ST_Solicitud>>(solicitudes));
        }

        /// <summary>
        /// Método que devuelve una solicitud por id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idSolicitud}", Name = "GetSolicitud")]
        public IActionResult GetSolicitud(decimal idSolicitud)
        {
            var solicitud = solicitudRepository.GetSolicitud(idSolicitud);
            if (solicitud == null) return NotFound();
            return Ok(mapper.Map<ST_Solicitud>(solicitud));
        }

        /// <summary>
        /// Método que permite actualizar una solicitud
        /// </summary>
        /// <param name="solicitudDTO"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateSolicitud([FromBody] ST_SolicitudDTO solicitudDTO)
        {
            var solicitud = mapper.Map<ST_Solicitud>(solicitudDTO);
            if (!solicitudRepository.UpdateSolicitud(solicitud))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar actualizar la solicitud {solicitudDTO.Id_Solicitud}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        /// <summary>
        /// Método que permite crear una nueva solicitud 
        /// </summary>
        /// <param name="solicitudDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateSolicitud([FromBody] ST_SolicitudDTO solicitudDTO)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var solicitud = mapper.Map<ST_Solicitud>(solicitudDTO);

            if (!solicitudRepository.CreateSolicitud(solicitud))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar registrar una nueva solicitud {solicitudDTO.Asunto_Sol}");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetSolicitud", new { idSolicitud = solicitud.Id_Solicitud }, solicitud);
        }

    }
}
