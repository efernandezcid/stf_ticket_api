﻿using ApiSantaFe.Models;
using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de movimientos (estados de solicitudes)
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MovimientoController : Controller
    {
        private readonly IMovimientoRepository movimientoRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de usuarios
        /// </summary>
        /// <param name="movimientoRepository"></param>
        /// <param name="mapper"></param> 
        public MovimientoController(IMovimientoRepository movimientoRepository, IMapper mapper)
        {
            this.movimientoRepository = movimientoRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve un movimiento por el id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idMovimiento}", Name = "GetMovimiento")]
        public IActionResult GetMovimiento(decimal idMovimiento)
        {
            var movimiento = movimientoRepository.GetMovimiento(idMovimiento);
            if (movimiento == null) return NotFound();
            return Ok(mapper.Map<ST_Movimiento>(movimiento));
        }


        /// <summary>
        /// Método que permite crear un movimiento de solicitud
        /// </summary>
        /// <param name="movimientoDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateMovimiento([FromBody] ST_MovimientoDTO movimientoDTO)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var movimiento = mapper.Map<ST_Movimiento>(movimientoDTO);

            if (!movimientoRepository.CreateMovimiento(movimiento))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar registrar un movimiento para la solicitud  {movimientoDTO.Id_Solicitud}");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetMovimiento", new { idMovimiento = movimiento.Id_Movimiento }, movimiento);
        }


    }
}
