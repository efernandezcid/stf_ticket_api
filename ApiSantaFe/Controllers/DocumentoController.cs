﻿using ApiSantaFe.Models;
using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiSantaFe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentoController : ControllerBase
    {

        private readonly IDocumentoRepository documentoRepository;
        private readonly IMapper mapper;

        public DocumentoController(IDocumentoRepository documentoRepository, IMapper mapper)
        {
            this.documentoRepository = documentoRepository;
            this.mapper = mapper;
        }

        // GET: api/<DocumentoController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // PUT api/<DocumentoController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<DocumentoController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost]
        public IActionResult CreateDocumento([FromBody] ST_DocumentoDTO documentoDTO)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var documento = mapper.Map<ST_Documento>(documentoDTO);

            if (!documentoRepository.CreateDocumento(documento, documentoDTO.Evidencia))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar registrar una nueva solicitud");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetDocumento", new { idDocumento = documento.IdDocumento }, documento);
        }

        [HttpGet("{idDocumento}", Name = "GetDocumento")]
        public async Task<IActionResult> GetDocumento(decimal idDocumento)
        {
            var documento = await documentoRepository.GetDocumento(idDocumento);
            if (documento == null) return NotFound();

            ST_DocumentoDTO documentoDto = mapper.Map<ST_DocumentoDTO>(documento);
            documentoDto.Evidencia = Convert.ToBase64String(documento.Documento);
            
            return Ok(documentoDto);
        }
    }
}
