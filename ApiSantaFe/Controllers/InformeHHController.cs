﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using ApiSantaFe.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de estados de usuario
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class InformeHHController : Controller
    {
        private readonly IInformeHHRepository informeHHRepository;
        private readonly IMapper mapper;
        private readonly IExcelEscrituraService excelEscrituraService;

        /// <summary>
        /// Constructor del controlador de estados de usuario
        /// </summary>
        /// <param name="informeHHRepository"></param>
        /// <param name="mapper"></param>
        public InformeHHController(IInformeHHRepository informeHHRepository, IMapper mapper, IExcelEscrituraService excelEscrituraService)
        {
            this.informeHHRepository = informeHHRepository;
            this.mapper = mapper;
            this.excelEscrituraService = excelEscrituraService;
        }

        /// <summary>
        /// Método que devuelve todos los posibles estados para un usuario 
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idArea}/{mes}/{ano}", Name = "GetInforme")]
        public IActionResult GetInforme(int idArea, int mes, int ano)
        {
            var informeHoras = informeHHRepository.GetInforme(idArea, mes, ano);
            return Ok(mapper.Map<List<InformeHH>>(informeHoras));
        }

        /// <summary>
        /// Método que genera y devuele un archivo excel con las horas hombres por mes
        /// </summary>
        /// <returns></returns>
        [HttpGet("Exportar/{idArea}/{mes}/{ano}")]
        public async Task<IActionResult> GetInformeHHExcel(int idArea, int mes, int ano)
        {
            List<ColumnaExcel> columnas = new List<ColumnaExcel>
            {
                new ColumnaExcel { KeyColumna = "Nombre_usuario", Nombre = "Nombre", Formato = FormatoExcel.Texto },
                new ColumnaExcel { KeyColumna ="Area_Descripcion", Nombre = "Centro de Costo", Formato = FormatoExcel.Texto },
                new ColumnaExcel { KeyColumna ="Total_Horas", Nombre = "Cantidad de Horas", Formato = FormatoExcel.Numero }
            };

            List<InformeHH> informeHHExcel = informeHHRepository.GetInforme(idArea, mes, ano).ToList();
            List<HojaExcel<InformeHH>> hojas = new List<HojaExcel<InformeHH>>();
            List<List<ColumnaExcel>> columnasExcel = new List<List<ColumnaExcel>>
            {
                columnas
            };

            string nombreMes = obtenerNombreMesNumero(mes);

            hojas.Add(new HojaExcel<InformeHH> { Nombre = $"Informe de Horas Hombre Mes {nombreMes}.", Data = informeHHExcel, Columnas = columnasExcel });


            return Ok(await excelEscrituraService.GeneraExcelKeyColumn(hojas));
        }

        private string obtenerNombreMesNumero(int numeroMes)
        {
            try
            {
                if (numeroMes == 1)
                {
                    return "Enero";
                }
                else if (numeroMes == 2)
                {
                    return "Febrero";
                }
                else if (numeroMes == 3)
                {
                    return "Marzo";
                }
                else if (numeroMes == 4)
                {
                    return "Abril";
                }
                else if (numeroMes == 5)
                {
                    return "Mayo";
                }
                else if (numeroMes == 6)
                {
                    return "Junio";
                }
                else if (numeroMes == 7)
                {
                    return "Julio";
                }
                else if (numeroMes == 8)
                {
                    return "Agosto";
                }
                else if (numeroMes == 9)
                {
                    return "Septiembre";
                }
                else if (numeroMes == 10)
                {
                    return "Octubre";
                }
                else if (numeroMes == 11)
                {
                    return "Noviembre";
                }
                else if (numeroMes == 12)
                {
                    return "Diciembre";
                }
                else
                {
                    return "Desconocido";
                }
            }
            catch
            {
                return "Desconocido";
            }
        }
    }

}
