﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de tipo de solicitud por area
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TipoSolicitudAreaController : Controller
    {
        private readonly ITipoSolicitudAreaRepository  tipoSolicitudAreaRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de tipos de solicitud por area
        /// </summary>
        /// <param name="tipoSolicitudAreaRepository"></param>
        /// <param name="mapper"></param>
        public TipoSolicitudAreaController(ITipoSolicitudAreaRepository tipoSolicitudAreaRepository, IMapper mapper)
        {
            this.tipoSolicitudAreaRepository = tipoSolicitudAreaRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve los tipos de solicitud por area
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idArea}", Name = "GetTiposSolicitudArea")]
        public IActionResult GetTiposSolicitudArea(decimal idArea)
        {
            var tiposSolicitudArea = tipoSolicitudAreaRepository.GetAllTiposSolicitudArea(idArea);
            if (tiposSolicitudArea == null) return NotFound();
            return Ok(mapper.Map<List<ST_Tipo_Solicitud>>(tiposSolicitudArea));
        }

    }
}
