﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de busqueda de usuario por mail
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LoginUsuarioController : Controller
    {
        private readonly IUsuarioRepository usuarioRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de usuarios
        /// </summary>
        /// <param name="usuarioRepository"></param>
        /// <param name="mapper"></param>
        public LoginUsuarioController(IUsuarioRepository usuarioRepository, IMapper mapper)
        {
            this.usuarioRepository = usuarioRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve un usuario por mail
        /// </summary>
        /// <returns></returns>
        [HttpGet("{mail}/{password}", Name = "GetLoginUsuario")]
        public IActionResult GetLoginUsuario(string mail, string password)
        {
            var respuesta = usuarioRepository.GetLoginUsuario(mail, password);
            if (respuesta == null) return NotFound();
            return Ok(mapper.Map<EstatusLogin>(respuesta));

        }
    }
}
