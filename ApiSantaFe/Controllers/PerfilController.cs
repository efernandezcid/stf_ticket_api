﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PerfilController : ControllerBase
    {
        private readonly IPerfilRepository perfilUsuarioRepository;
        private readonly IMapper mapper;

        public PerfilController(IPerfilRepository perfilUsuarioRepository, IMapper mapper)
        {
            this.perfilUsuarioRepository = perfilUsuarioRepository;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAllPerfilUsuario()
        {
            var perfilUsuario = perfilUsuarioRepository.GetAllPerfilesUsuario();
            return Ok(mapper.Map<List<ST_Perfil_Usuario>>(perfilUsuario));
        }
    }
}
