﻿using ApiSantaFe.Models;
using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de áreas
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AreaController : Controller
    {
        private readonly IAreaRepository areaRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de areas
        /// </summary>
        /// <param name="areaRepository"></param>
        /// <param name="mapper"></param>
        public AreaController(IAreaRepository areaRepository, IMapper mapper)
        {
            this.areaRepository = areaRepository;
            this.mapper = mapper;
        }


        /// <summary>
        /// Método que devuelve todas las areas existentes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllAreas()
        {
            var areas = areaRepository.GetAllAreas();
            return Ok(mapper.Map<List<ST_AreaDTO>>(areas));
        }

        /// <summary>
        /// Método que devuelve un area en especifico
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idArea}", Name = "GetArea")]
        public IActionResult GetArea(decimal idArea)
        {
            var area = areaRepository.GetArea(idArea);
            if (area == null) return NotFound();
            return Ok(mapper.Map<ST_AreaDTO>(area));
        }

        /// <summary>
        /// Método que permite actualizar un area en la base de datos
        /// </summary>
        /// <param name="areaDTO"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult UpdateArea([FromBody] ST_AreaDTO areaDTO)
        {
            var area = mapper.Map<ST_Area>(areaDTO);
            if (!areaRepository.UpdateArea(area))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar actualizar el area {areaDTO.Dsc_Area}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }


    }




}
