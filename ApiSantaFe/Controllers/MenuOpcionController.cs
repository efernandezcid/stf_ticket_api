﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MenuOpcionController : Controller
    {
        private readonly IMenuOpcionRepository menuOpcionRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuOpcionRepository"></param>
        /// <param name="mapper"></param>
        public MenuOpcionController(IMenuOpcionRepository menuOpcionRepository, IMapper mapper)
        {
            this.menuOpcionRepository = menuOpcionRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllMenuOpcion()
        {
            var menuOpciones = menuOpcionRepository.GetAllMenuOpcion();
            return Ok(mapper.Map<List<ST_Menu_Opcion>>(menuOpciones));
        }
    }
}
