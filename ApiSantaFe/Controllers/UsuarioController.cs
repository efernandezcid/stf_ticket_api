﻿using ApiSantaFe.Models;
using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de usuarios
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioRepository usuarioRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de usuarios
        /// </summary>
        /// <param name="usuarioRepository"></param>
        /// <param name="mapper"></param>
        public UsuarioController(IUsuarioRepository usuarioRepository, IMapper mapper)
        {
            this.usuarioRepository = usuarioRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve todos los usuarios de la base de datos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllUsuarios()
        {
            var usuarios = usuarioRepository.GetAllUsuarios();
            return Ok(mapper.Map<List<ST_Usuario>>(usuarios));
        }

        /// <summary>
        /// Método que devuelve un usuario por id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idUsuario}", Name = "GetUsuario")]
        public IActionResult GetUsuario(decimal idUsuario)
        {
            var usuario = usuarioRepository.GetUsuario(idUsuario);
            if (usuario == null) return NotFound();
            return Ok(mapper.Map<ST_Usuario>(usuario));
        }

        /// <summary>
        /// Método que permite actualizar un usuario en la base de datos
        /// </summary>

        /// <param name="usuarioDTO"></param>
        /// <returns></returns>
        [HttpPut(Name = "UpdateUsuario")]
        public IActionResult UpdateUsuario([FromBody] ST_UsuarioDTO usuarioDTO)
        {
            var usuario = mapper.Map<ST_Usuario>(usuarioDTO);
            
            if (!usuarioRepository.UpdateUsuario(usuario, usuarioDTO.Password))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar actualizar el usuario {usuarioDTO.Nombre_Usr}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        /// <summary>
        /// Método que permite crear un nuevo usuario en la base de datos 
        /// </summary>
        /// <param name="usuarioDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateUsuario([FromBody] ST_UsuarioDTO usuarioDTO)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            if (usuarioRepository.UsuarioExists(usuarioDTO.Email_Usr))
            {
                ModelState.AddModelError(string.Empty, $"ya existe un usuario con este correo {usuarioDTO.Email_Usr}");
                return StatusCode(404, ModelState);
            }

            var usuario = mapper.Map<ST_Usuario>(usuarioDTO);

            if (!usuarioRepository.CreateUsuario(usuario, usuarioDTO.Password))
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error al intentar registrar un nuevo usuario {usuarioDTO.Nombre_Usr}");
                return StatusCode(500, ModelState);
            }

            return CreatedAtRoute("GetUsuario", new { idUsuario = usuario.Id_Usuario }, usuario);
        }


    }
}
