﻿using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de estados de usuario
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoUsuarioController : Controller
    {
        private readonly IEstadoUsuarioRepository estadoUsuarioRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de estados de usuario
        /// </summary>
        /// <param name="estadoUsuarioRepository"></param>
        /// <param name="mapper"></param>
        public EstadoUsuarioController(IEstadoUsuarioRepository estadoUsuarioRepository, IMapper mapper)
        {
            this.estadoUsuarioRepository = estadoUsuarioRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve todos los posibles estados para un usuario 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllEstadosUsuario()
        {
            var estadosUsuario = estadoUsuarioRepository.GetAllEstadosUsuario();
            return Ok(mapper.Map<List<ST_Estado_UsrDTO>>(estadosUsuario));
        }

    }
}
