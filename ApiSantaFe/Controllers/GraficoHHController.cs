﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de estados de usuario
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class GraficoHHController : Controller
    {
        private readonly IGraficoHHRepository graficoRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de usuarios
        /// </summary>
        /// <param name="graficoRepository"></param>
        /// <param name="mapper"></param>
        public GraficoHHController(IGraficoHHRepository graficoRepository, IMapper mapper)
        {
            this.graficoRepository = graficoRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve los usuarios por un área determinada
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idArea}/{mes}/{ano}", Name = "GetDatosGrafico")]
        public IActionResult GetDatosGrafico(int idArea, int mes, int ano)
        {
            var datosGrafico = graficoRepository.GetDatosGrafico(idArea,mes, ano);
            return Ok(mapper.Map<List<GraficoHH>>(datosGrafico));
        }
    }
}
