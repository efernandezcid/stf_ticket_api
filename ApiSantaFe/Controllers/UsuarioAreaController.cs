﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de busqueda de usuario por mail
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioAreaController : Controller
    {
        private readonly IUsuarioRepository usuarioRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de usuarios
        /// </summary>
        /// <param name="usuarioRepository"></param>
        /// <param name="mapper"></param>
        public UsuarioAreaController(IUsuarioRepository usuarioRepository, IMapper mapper)
        {
            this.usuarioRepository = usuarioRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve los usuarios por un área determinada
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id_Area}", Name = "GetUsuarioArea")]
        public IActionResult GetUsuarioArea(decimal id_Area)
        {
            var usuarios = usuarioRepository.GetUsuarioArea(id_Area);
            if (usuarios == null) return NotFound();
            return Ok(mapper.Map<List<ST_Usuario>>(usuarios));
        }
    }
}
