﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de tipos de solicitud
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TipoSolicitudController : Controller
    {
        private readonly ITipoSolicitudRepository tipoSolicitudRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de tipos de solicitud
        /// </summary>
        /// <param name="tipoSolicitudRepository"></param>
        /// <param name="mapper"></param>
        public TipoSolicitudController(ITipoSolicitudRepository tipoSolicitudRepository, IMapper mapper)
        {
            this.tipoSolicitudRepository = tipoSolicitudRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve todos los tipos de solicitud
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllTipoSolicitud()
        {
            var tiposSolicitud = tipoSolicitudRepository.GetAllTiposSolicitud();
            return Ok(mapper.Map<List<ST_Tipo_Solicitud>>(tiposSolicitud));
        }

        /// <summary>
        /// Método que devuelve un tipo de solicitud 
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idTipoSolicitud}", Name = "GetTipoSolicitud")]
        public IActionResult GetTipoSolicitud(decimal idTipoSolicitud)
        {
            var tipoSolicitud = tipoSolicitudRepository.GetTipoSolicitud(idTipoSolicitud);
            if (tipoSolicitud == null) return NotFound();
            return Ok(mapper.Map<ST_Tipo_Solicitud>(tipoSolicitud));
        }

    }
}
