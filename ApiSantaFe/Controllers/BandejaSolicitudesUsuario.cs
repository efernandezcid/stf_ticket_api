﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de bandeja de solicitudes por usuario
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class BandejaSolicitudesUsuario : Controller
    {
        private readonly IMovimientoRepository movimientoRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de usuarios
        /// </summary>
        /// <param name="movimientoRepository"></param>
        /// <param name="mapper"></param>
        public BandejaSolicitudesUsuario(IMovimientoRepository movimientoRepository, IMapper mapper)
        {
            this.movimientoRepository = movimientoRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve las solicitudes en estado ingresada por area
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idusuario}", Name = "GetSolicitudesBandejaUsuario")]
        public IActionResult GetSolicitudesBandejaUsuario(decimal idusuario)
        {
            var movimientos = movimientoRepository.GetMovimientosBandejaUsuario(idusuario);
            //if (movimientos.Count() == 0) return NotFound();
            return Ok(mapper.Map<List<ST_Movimiento>>(movimientos));
        }


    }
}
