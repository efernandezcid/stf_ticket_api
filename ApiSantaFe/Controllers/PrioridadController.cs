﻿using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de prioridades
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PrioridadController : Controller
    {
        private readonly IPrioridadRepository prioridadRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor del controlador de prioridad
        /// </summary>
        /// <param name="prioridadRepository"></param>
        /// <param name="mapper"></param>
        public PrioridadController(IPrioridadRepository prioridadRepository, IMapper mapper)
        {
            this.prioridadRepository = prioridadRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve todas las posibles prioridades de una solicitud
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllPrioridades()
        {
            var prioridad = prioridadRepository.GetAllPrioridades();
            return Ok(mapper.Map<List<ST_PrioridadDTO>>(prioridad));
        }

    }
}
