﻿using ApiSantaFe.Models.DTO;
using ApiSantaFe.Repository;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// Controlador de estados de solicitudes
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class EstadoSolicitudController : Controller
    {
        private readonly IEstadoSolicitudRepository estadoSolicitudRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// Constructor de estados de solicitud 
        /// </summary>
        /// <param name="estadoSolicitudRepository"></param>
        /// <param name="mapper"></param>
        public EstadoSolicitudController(IEstadoSolicitudRepository estadoSolicitudRepository, IMapper mapper)
        {
            this.estadoSolicitudRepository = estadoSolicitudRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve todos los estados de una solicitud
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllEstadosSolicitud()
        {
            var estadosSolicitud = estadoSolicitudRepository.GetAllEstadosSolicitud();
            return Ok(mapper.Map<List<ST_Estado_SolDTO>>(estadosSolicitud));
        }

    }
}
