﻿using ApiSantaFe.Models;
using ApiSantaFe.Repository.IRepository;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSantaFe.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MenuOpcionDependenciaController : Controller
    {
        private readonly IMenuOpcionRepository menuOpcionRepository;
        private readonly IMapper mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuOpcionRepository"></param>
        /// <param name="mapper"></param>
        public MenuOpcionDependenciaController(IMenuOpcionRepository menuOpcionRepository, IMapper mapper)
        {
            this.menuOpcionRepository = menuOpcionRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Método que devuelve los menu dependiendo de padre de estos
        /// </summary>
        /// <returns></returns>
        [HttpGet("{idPadre}", Name = "GetAllMenuOpcionDependencia")]
        public IActionResult GetAllMenuOpcionDependencia(decimal idPadre)
        {
            var menuOpciones = menuOpcionRepository.GetMenuOpcionDependencia(idPadre);
            return Ok(mapper.Map<List<ST_Menu_Opcion>>(menuOpciones));
        }
    }
}
