﻿using System;
using System.Linq;
using System.Data;
using OfficeOpenXml;
using System.Drawing;
using OfficeOpenXml.Style;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Dynamic;

namespace ApiSantaFe.Services
{
    public interface IExcelEscrituraService
    {
        Task<string> GeneraExcel<T>(List<HojaExcel<T>> hojas);
        Task<string> GeneraExcelKeyColumn<T>(List<HojaExcel<T>> hojas);
        Task<string> GeneraExcelVrs3(List<HojaExcelDataTable> hojas);
        Task<string> GeneraExcelDistribucionFondo(List<HojaExcelDistribucionfondo> hojas);
        Task<string> GeneraExcelAllDistribucionFondo(List<HojaExcelAllDistribucionfondo> hojas);

        string GenerarExcelSimple<T>(List<HojaExcel<T>> hojas);
        string GeneraExcelConGrupos<T, K>(List<HojaExcel<T>> hojas, List<ColumnaExcel> columnasDetalle, List<GrupoExcel<K>> grupoDetalle);
        string ToInitcap(string str);
    }

    public class ExcelEscrituraService : IExcelEscrituraService
    {
        private readonly int rowEncab = 8;

        public async Task<string> GeneraExcel<T>(List<HojaExcel<T>> hojas)
        {
            byte[] excelRep;
            string stream;

            excelRep = CreateExcelEpplus(hojas);
            stream = Convert.ToBase64String(excelRep, 0, excelRep.Length);
            return await Task.FromResult(stream);
        }

        public async Task<string> GeneraExcelKeyColumn<T>(List<HojaExcel<T>> hojas)
        {
            byte[] excelRep;
            string stream;

            excelRep = CreateExcelEpplusVrs2(hojas);
            stream = Convert.ToBase64String(excelRep, 0, excelRep.Length);
            return await Task.FromResult(stream);
        }

        public async Task<string> GeneraExcelVrs3(List<HojaExcelDataTable> hojas)
        {
            byte[] excelRep;
            string stream;

            excelRep = CreateExcelEpplusVrs3(hojas);
            stream = Convert.ToBase64String(excelRep, 0, excelRep.Length);
            return await Task.FromResult(stream);
        }
        public async Task<string> GeneraExcelDistribucionFondo(List<HojaExcelDistribucionfondo> hojas)
        {
            byte[] excelRep;
            string stream;

            excelRep = CreateExcelDistFondo(hojas);
            stream = Convert.ToBase64String(excelRep, 0, excelRep.Length);
            return await Task.FromResult(stream);
        }
        public async Task<string> GeneraExcelAllDistribucionFondo(List<HojaExcelAllDistribucionfondo> hojas)
        {
            byte[] excelRep;
            string stream;

            excelRep = CreateExcelAllDistFondo(hojas);
            stream = Convert.ToBase64String(excelRep, 0, excelRep.Length);
            return await Task.FromResult(stream);
        }

        public string GenerarExcelSimple<T>(List<HojaExcel<T>> hojas)
        {
            byte[] excelRep;
            string stream;

            excelRep = CreateExcelSimpleEpplus(hojas);
            stream = Convert.ToBase64String(excelRep, 0, excelRep.Length);
            return stream;
        }

        public byte[] CreateExcelDistFondo(List<HojaExcelDistribucionfondo> hojas)
        {
            ExcelPackage p = new ExcelPackage();
            SetWorkbookProperties(ref p);

            ExcelWorksheet ws;
            int nroHoja = 0;

            foreach (var hoja in hojas)
            {
                ws = CreateSheet(p, "Distribución", nroHoja++);
                int rowIndex = rowEncab;
                int nroColumna = 2;

                foreach (var columna in hoja.Columnas)
                {
                    CreateHeader(ref ws, ref rowIndex, columna);
                }

                CreateDataDistFondo(ref ws, ref rowIndex, hoja, ref nroColumna);
                CreateTitleDistFondo(ref ws, nroColumna, hoja);
                CreateFooter(ref ws, nroColumna);
            }

            return p.GetAsByteArray();
        }

        public byte[] CreateExcelAllDistFondo(List<HojaExcelAllDistribucionfondo> hojas)
        {
            ExcelPackage p = new ExcelPackage();
            SetWorkbookProperties(ref p);

            ExcelWorksheet ws;
            int nroHoja = 0;

            foreach (var hoja in hojas)
            {
                ws = CreateSheet(p, "Distribución", nroHoja++);
                int rowIndex = rowEncab;
                int nroColumna = 2;

                foreach (var columna in hoja.Columnas)
                {
                    CreateHeader(ref ws, ref rowIndex, columna);
                }

                CreateDataAllDistFondo(ref ws, ref rowIndex, hoja, ref nroColumna);
                CreateTitleAllDistFondo(ref ws, nroColumna, hoja);
                CreateFooter(ref ws, nroColumna);
            }

            return p.GetAsByteArray();
        }

        public byte[] CreateExcelEpplusVrs3(List<HojaExcelDataTable> hojas)
        {
            ExcelPackage p = new ExcelPackage();
            SetWorkbookProperties(ref p);

            ExcelWorksheet ws;
            int nroHoja = 0;

            foreach (var hoja in hojas)
            {
                ws = CreateSheet(p, hoja.Nombre, nroHoja++);
                int rowIndex = rowEncab;
                int nroColumna = 2;

                foreach (var columna in hoja.Columnas)
                {
                    CreateHeader(ref ws, ref rowIndex, columna);
                }

                CreateDataVrs3(ref ws, ref rowIndex, hoja, ref nroColumna);
                CreateTitleVrs3(ref ws, nroColumna, hoja);
                CreateFooter(ref ws, nroColumna);
            }

            return p.GetAsByteArray();
        }

        public byte[] CreateExcelEpplusVrs2<T>(List<HojaExcel<T>> hojas)
        {
            ExcelPackage p = new ExcelPackage();
            SetWorkbookProperties(ref p);

            ExcelWorksheet ws;
            int nroHoja = 0;

            foreach (var hoja in hojas)
            {
                ws = CreateSheet(p, hoja.Nombre, nroHoja++);
                int rowIndex = rowEncab;
                int nroColumna = 2;

                foreach (var columna in hoja.Columnas)
                {
                    CreateHeader(ref ws, ref rowIndex, columna);
                }

                CreateDataVrs2(ref ws, ref rowIndex, hoja, ref nroColumna);
                CreateTitle(ref ws, nroColumna, hoja);
                CreateFooter(ref ws, nroColumna);
            }

            return p.GetAsByteArray();
        }

        public byte[] CreateExcelEpplus<T>(List<HojaExcel<T>> hojas)
        {
            ExcelPackage p = new ExcelPackage();
            SetWorkbookProperties(ref p);

            ExcelWorksheet ws;
            int nroHoja = 0;

            foreach (var hoja in hojas)
            {
                ws = CreateSheet(p, hoja.Nombre, nroHoja++);
                int rowIndex = rowEncab;
                int nroColumna = 2;

                foreach (var columna in hoja.Columnas)
                {
                    CreateHeader(ref ws, ref rowIndex, columna);
                }

                var maxData = hoja.Columnas.Count - 1;
                CreateData(ref ws, ref rowIndex, hoja.Data, hoja.Columnas[maxData], ref nroColumna);
                CreateTitle(ref ws, nroColumna, hoja);
                CreateFooter(ref ws, nroColumna);
            }

            return p.GetAsByteArray();
        }

        public byte[] CreateExcelSimpleEpplus<T>(List<HojaExcel<T>> hojas)
        {
            ExcelPackage p = new ExcelPackage();
            SetWorkbookProperties(ref p);

            ExcelWorksheet ws;
            int nroHoja = 0;

            foreach (var hoja in hojas)
            {
                ws = CreateSheet(p, hoja.Nombre, nroHoja++);
                int rowIndex = 1;
                int nroColumna = 1;

                List<ColumnaExcel> columnas = hoja.Columnas.FirstOrDefault();
                CreateHeaderSimple(ref ws, ref rowIndex, columnas);

                var ultimaColumna = hoja.Columnas.Count - 1;
                CreateData(ref ws, ref rowIndex, hoja.Data, hoja.Columnas[ultimaColumna], ref nroColumna);

                ExcelRange RangoCompleto = ws.Cells[1, 1, ws.Dimension.End.Row, ws.Dimension.End.Column];
                if (hoja.FilaInmobilizacion.HasValue)
                {
                    ws.View.FreezePanes(hoja.FilaInmobilizacion.Value, 1);
                    ws.Protection.IsProtected = true;
                    for (int i = 1; i <= columnas.Count; i++)
                    {
                        ws.Column(i).Style.Locked = false;
                    }
                    for (int j = 1; j < hoja.FilaInmobilizacion.Value; ++j)
                    {
                        ws.Row(j).Style.Locked = true;
                    }
                }

                RangoCompleto.AutoFitColumns();
            }

            return p.GetAsByteArray();
        }

        private static void SetWorkbookProperties(ref ExcelPackage p)
        {
            p.Workbook.Properties.Author = "Creasys";
            p.Workbook.Properties.Title = "Creasys Excel Documents";
        }

        private ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName, int nroHoja)
        {
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[nroHoja];
            ws.Name = sheetName;
            ws.Cells.Style.Font.Size = 11;
            ws.Cells.Style.Font.Name = "Arial";
            ws.View.ShowGridLines = false;

            return ws;
        }

        private void CreateTitleVrs3(ref ExcelWorksheet ws, int columnas, HojaExcelDataTable item)
        {
            ws.Cells[2, 2].Value = item.Titulo ?? item.Nombre;
            ws.Cells[2, 2, 2, columnas - 1].Merge = true;
            ws.Cells[2, 2, 2, columnas - 1].Style.Font.Size = 16;
            ws.Cells[1, 2, 2, columnas - 1].Style.Font.Bold = true;
            ws.Cells[1, 2, 2, columnas - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[5, 2].Value = $"Fecha de generación: {DateTime.Today:dd-MM-yyyy}";
            ws.Cells[5, 2].Style.Font.Bold = true;

            ws.Cells[6, 2].Value = item.Negocio == null ? "" : $"Negocio: {item.Negocio}";
            ws.Cells[6, 2].Style.Font.Bold = true;

            string fechaDH = "";
            if (item.Desde.IsNotNull())
            {
                fechaDH = $"Fecha desde: {item.Desde?.ToString("dd-MM-yyyy")}";
            }

            if (item.Hasta.IsNotNull())
            {
                fechaDH += $" Hasta: {item.Hasta?.ToString("dd-MM-yyyy")}";
            }

            if (item.FechaConsulta.IsNotNull())
            {
                fechaDH = $"Fecha consulta: {item.FechaConsulta?.ToString("dd-MM-yyyy")}";
            }

            ws.Cells[7, 2].Value = fechaDH;
            ws.Cells[7, 2].Style.Font.Bold = true;
        }

        private void CreateTitleDistFondo(ref ExcelWorksheet ws, int columnas, HojaExcelDistribucionfondo item)
        {
            ws.Cells[2, 2].Value = item.Titulo ?? item.Descripcion;
            ws.Cells[2, 2, 2, columnas - 1].Merge = true;
            ws.Cells[2, 2, 2, columnas - 1].Style.Font.Size = 16;
            ws.Cells[1, 2, 2, columnas - 1].Style.Font.Bold = true;
            ws.Cells[1, 2, 2, columnas - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[3, 2].Value = $"Fecha Inicio Vigencia: {item.FechaIncioVgn.ToString("dd-MM-yyyy")}";
            ws.Cells[3, 2].Style.Font.Bold = true;

            if ((Convert.ToDateTime(item.FechaFinVgn).ToString("dd-MM-yyyy") == "01-01-0001"))
            {
                ws.Cells[4, 2].Value = $"Fecha fin Vigencia: { "--" }";
                ws.Cells[4, 2].Style.Font.Bold = true;
            }
            else
            {
                ws.Cells[4, 2].Value = $"Fecha fin Vigencia: { Convert.ToDateTime(item.FechaFinVgn).ToString("dd-MM-yyyy") }";
                ws.Cells[4, 2].Style.Font.Bold = true;
            }


            ws.Cells[5, 2].Value = $"Estado: {item.Estado}";
            ws.Cells[5, 2].Style.Font.Bold = true;

            ws.Cells[6, 2].Value = $"Descripción Fondo: {item.Descripcion}";
            ws.Cells[6, 2].Style.Font.Bold = true;


        }

        private void CreateTitleAllDistFondo(ref ExcelWorksheet ws, int columnas, HojaExcelAllDistribucionfondo item)
        {
            ws.Cells[2, 2].Value = "Distribución Fondo";//item.Titulo ?? item.Descripcion;
            ws.Cells[2, 2, 2, columnas - 1].Merge = true;
            ws.Cells[2, 2, 2, columnas - 1].Style.Font.Size = 16;
            ws.Cells[1, 2, 2, columnas - 1].Style.Font.Bold = true;
            ws.Cells[1, 2, 2, columnas - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

        }


        private void CreateTitle<T>(ref ExcelWorksheet ws, int columnas, HojaExcel<T> item)
        {
            ws.Cells[2, 2].Value = item.Titulo ?? item.Nombre;
            ws.Cells[2, 2, 2, columnas - 1].Merge = true;
            ws.Cells[2, 2, 2, columnas - 1].Style.Font.Size = 16;
            ws.Cells[1, 2, 2, columnas - 1].Style.Font.Bold = true;
            ws.Cells[1, 2, 2, columnas - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[5, 2].Value = $"Fecha de generación: {DateTime.Today:dd-MM-yyyy}";
            ws.Cells[5, 2].Style.Font.Bold = true;

            ws.Cells[6, 2].Value = item.Negocio == null ? "" : $"Negocio: {item.Negocio}";
            ws.Cells[6, 2].Style.Font.Bold = true;

            string fechaDH = "";
            if (item.Desde.IsNotNull())
            {
                fechaDH = $"Fecha desde: {item.Desde?.ToString("dd-MM-yyyy")}";
            }

            if (item.Hasta.IsNotNull())
            {
                fechaDH += $" Hasta: {item.Hasta?.ToString("dd-MM-yyyy")}";
            }

            if (item.FechaConsulta.IsNotNull())
            {
                fechaDH = $"Fecha consulta: {item.FechaConsulta?.ToString("dd-MM-yyyy")}";
            }

            ws.Cells[7, 2].Value = fechaDH;
            ws.Cells[7, 2].Style.Font.Bold = true;
        }

        private void CreateHeader(ref ExcelWorksheet ws, ref int rowIndex, List<ColumnaExcel> columnas)
        {
            int colIndex = 1;
            ExcelRange cell;
            ExcelFill fill;
            Border Border;
            rowIndex += 1;
            int colMerge = 1;

            foreach (var col in columnas)
            {
                colIndex += 1;

                cell = ws.Cells[rowIndex, colIndex];
                fill = cell.Style.Fill;
                Border = cell.Style.Border;
                cell.Style.Font.Color.SetColor(Color.White);

                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.FromArgb(236, 0, 0));

                cell.Value = col.Nombre.ToString().Replace("</br>", " ");

                switch (col.Formato)
                {
                    case FormatoExcel.Merge:
                        colMerge += 1;
                        break;
                    case FormatoExcel.AplicaMerge:
                        ws.Cells[rowIndex, colIndex - colMerge, rowIndex, colIndex].Merge = true;
                        ws.Cells[rowIndex, colIndex - colMerge, rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        colMerge = 1;
                        break;
                    case FormatoExcel.Numero:
                    case FormatoExcel.Moneda:
                        ws.Cells[rowIndex, colIndex, rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        break;
                }
            }
        }

        private void CreateHeaderSimple(ref ExcelWorksheet ws, ref int rowIndex, List<ColumnaExcel> columnas)
        {
            int colIndex = 1;
            ExcelRange cell;
            rowIndex = 1;
            int colMerge = 1;

            foreach (var col in columnas)
            {
                cell = ws.Cells[rowIndex, colIndex];
                cell.Value = col.Nombre.ToString();

                switch (col.Formato)
                {
                    case FormatoExcel.Merge:
                        colMerge += 1;
                        break;
                    case FormatoExcel.AplicaMerge:
                        ws.Cells[rowIndex, colIndex - colMerge, rowIndex, colIndex].Merge = true;
                        ws.Cells[rowIndex, colIndex - colMerge, rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        colMerge = 1;
                        break;
                    case FormatoExcel.Numero:
                    case FormatoExcel.Moneda:
                        ws.Cells[rowIndex, colIndex, rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        break;
                }
                colIndex += 1;
            }
        }

        private void CreateData<T>(ref ExcelWorksheet ws, ref int rowindex, List<T> data, List<ColumnaExcel> columnas, ref int nroColumna, Color? fillColor = null)
        {
            ExcelRange cell = ws.Cells[++rowindex, nroColumna];

            cell.LoadFromCollection(data);

            nroColumna = FormatearExcel(ws, rowindex, nroColumna, fillColor, columnas);

            rowindex = ws.Dimension.End.Row;
        }

        private void CreateDataVrs2<T>(ref ExcelWorksheet ws, ref int rowindex, HojaExcel<T> hoja, ref int nroColumna, Color? fillColor = null)
        {
            var maxData = hoja.Columnas.Count - 1;
            List<ColumnaExcel> columnas = hoja.Columnas[maxData];
            ExcelRange cell = ws.Cells[++rowindex, nroColumna];

            DataTable datatable = ListToDataTable(hoja.Data, columnas);

            cell.LoadFromDataTable(datatable, false);

            if (hoja.AgregaFila.IsNotNull())
            {
                int inicio = rowEncab + 2;
                for (int i = 0; i < hoja.Data.Count; i += hoja.AgregaFila.CadaCuanto)
                {
                    inicio += hoja.AgregaFila.CadaCuanto;
                    ws.InsertRow(inicio, hoja.AgregaFila.LineasAgregar);
                    inicio += hoja.AgregaFila.LineasAgregar;
                }
            }

            nroColumna = FormatearExcel(ws, rowindex, nroColumna, fillColor, columnas);

            rowindex = ws.Dimension.End.Row;
        }

        private void CreateDataDistFondo(ref ExcelWorksheet ws, ref int rowindex, HojaExcelDistribucionfondo hoja, ref int nroColumna, Color? fillColor = null)
        {
            var maxData = hoja.Columnas.Count - 1;
            List<ColumnaExcel> columnas = hoja.Columnas[maxData];
            ExcelRange cell = ws.Cells[++rowindex, nroColumna];

            cell.LoadFromDataTable(hoja.Data, false);

            if (hoja.AgregaFila.IsNotNull())
            {
                int inicio = rowEncab + 2;
                for (int i = 0; i < hoja.Data.ToDynamicList().Count; i += hoja.AgregaFila.CadaCuanto)
                {
                    inicio += hoja.AgregaFila.CadaCuanto;
                    ws.InsertRow(inicio, hoja.AgregaFila.LineasAgregar);
                    inicio += hoja.AgregaFila.LineasAgregar;
                }
            }

            nroColumna = FormatearExcel(ws, rowindex, nroColumna, fillColor, columnas);

            rowindex = ws.Dimension.End.Row;
        }

        private void CreateDataAllDistFondo(ref ExcelWorksheet ws, ref int rowindex, HojaExcelAllDistribucionfondo hoja, ref int nroColumna, Color? fillColor = null)
        {
            var maxData = hoja.Columnas.Count - 1;
            List<ColumnaExcel> columnas = hoja.Columnas[maxData];
            ExcelRange cell = ws.Cells[++rowindex, nroColumna];

            cell.LoadFromDataTable(hoja.Data, false);

            if (hoja.AgregaFila.IsNotNull())
            {
                int inicio = rowEncab + 2;
                for (int i = 0; i < hoja.Data.ToDynamicList().Count; i += hoja.AgregaFila.CadaCuanto)
                {
                    inicio += hoja.AgregaFila.CadaCuanto;
                    ws.InsertRow(inicio, hoja.AgregaFila.LineasAgregar);
                    inicio += hoja.AgregaFila.LineasAgregar;
                }
            }

            nroColumna = FormatearExcel(ws, rowindex, nroColumna, fillColor, columnas);

            rowindex = ws.Dimension.End.Row;
        }


        private void CreateDataVrs3(ref ExcelWorksheet ws, ref int rowindex, HojaExcelDataTable hoja, ref int nroColumna, Color? fillColor = null)
        {
            var maxData = hoja.Columnas.Count - 1;
            List<ColumnaExcel> columnas = hoja.Columnas[maxData];
            ExcelRange cell = ws.Cells[++rowindex, nroColumna];

            cell.LoadFromDataTable(hoja.Data, false);

            if (hoja.AgregaFila.IsNotNull())
            {
                int inicio = rowEncab + 2;
                for (int i = 0; i < hoja.Data.ToDynamicList().Count; i += hoja.AgregaFila.CadaCuanto)
                {
                    inicio += hoja.AgregaFila.CadaCuanto;
                    ws.InsertRow(inicio, hoja.AgregaFila.LineasAgregar);
                    inicio += hoja.AgregaFila.LineasAgregar;
                }
            }

            nroColumna = FormatearExcel(ws, rowindex, nroColumna, fillColor, columnas);

            rowindex = ws.Dimension.End.Row;
        }

        private static int FormatearExcel(ExcelWorksheet ws, int rowindex, int nroColumna, Color? fillColor, List<ColumnaExcel> columnas)
        {
            if (fillColor.HasValue)
            {
                ExcelFill fill = ws.Cells[rowindex, ws.Dimension.Start.Column + 1, ws.Dimension.End.Row, ws.Dimension.End.Column].Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(fillColor.Value);
            }

            foreach (var col in columnas)
            {
                string cantDecimales = "";

                if (col.NroDecimales > 0)
                {
                    cantDecimales = "." + cantDecimales.PadRight((int)col.NroDecimales, '0');
                }

                switch (col.Formato)
                {
                    case FormatoExcel.Oculto:
                        ws.DeleteColumn(nroColumna--);
                        break;
                    case FormatoExcel.Moneda:
                        ws.Cells[rowindex, nroColumna, ws.Dimension.End.Row, nroColumna].Style.Numberformat.Format = "$ #,##0" + cantDecimales;
                        break;
                    case FormatoExcel.Numero:
                        ws.Cells[rowindex, nroColumna, ws.Dimension.End.Row, nroColumna].Style.Numberformat.Format = "#,##0" + cantDecimales;
                        ws.Cells[rowindex, nroColumna, ws.Dimension.End.Row, nroColumna].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        break;
                    case FormatoExcel.Porcentaje:
                        ws.Cells[rowindex, nroColumna, ws.Dimension.End.Row, nroColumna].Style.Numberformat.Format = "0.00%";
                        break;
                    case FormatoExcel.Fecha:
                        ws.Cells[rowindex, nroColumna, ws.Dimension.End.Row, nroColumna].Style.Numberformat.Format = "dd/mm/yyyy";
                        ws.Cells[rowindex, nroColumna, ws.Dimension.End.Row, nroColumna].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        break;
                    case FormatoExcel.FechaConHora:
                        ws.Cells[rowindex, nroColumna, ws.Dimension.End.Row, nroColumna].Style.Numberformat.Format = "dd/mm/yyyy hh:mm";
                        break;
                }
                nroColumna++;
            }

            return nroColumna;
        }

        public static DataTable ListToDataTable<T>(List<T> data, List<ColumnaExcel> columnas)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dataTable = new DataTable();

            for (int i = 0; i < columnas.Count; i++)
            {
                var llave = columnas[i].KeyColumna ?? columnas[i].Nombre;
                PropertyDescriptor property = properties.Find(llave, false);
                if (property.IsNotNull())
                {
                    dataTable.Columns.Add(property.Name, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
                }
            }

            object[] values = new object[dataTable.Columns.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    var nombre = dataTable.Columns[i].ColumnName;
                    PropertyDescriptor property = properties.Find(nombre, false);
                    values[i] = property.GetValue(item);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

        private void CreateFooter(ref ExcelWorksheet ws, int columna)
        {
            int totalCol = ws.Dimension.End.Column + 1;

            for (int i = columna; i <= totalCol; i++)
            {
                ws.DeleteColumn(columna);
            }

            ExcelRange RangoCompleto = ws.Cells[rowEncab, 2, ws.Dimension.End.Row, ws.Dimension.End.Column];
            Border border = RangoCompleto.Style.Border;
            border.Bottom.Style = ExcelBorderStyle.Thin;
            border.Bottom.Color.SetColor(Color.FromArgb(191, 191, 191));
            RangoCompleto.AutoFitColumns();
        }

        public string ToInitcap(string texto)
        {
            if (string.IsNullOrEmpty(texto))
            {
                return texto;
            }

            char[] charArray = new char[texto.Length];
            bool newWord = true;

            for (int i = 0; i < texto.Length; ++i)
            {
                Char currentChar = texto[i];
                if (Char.IsLetter(currentChar))
                {
                    if (newWord)
                    {
                        newWord = false;
                        currentChar = Char.ToUpper(currentChar);
                    }
                    else
                    {
                        currentChar = Char.ToLower(currentChar);
                    }
                }
                else if (Char.IsWhiteSpace(currentChar))
                {
                    newWord = true;
                }
                charArray[i] = currentChar;
            }

            return new string(charArray);
        }

        public string GeneraExcelConGrupos<T, K>(List<HojaExcel<T>> hojas, List<ColumnaExcel> columnasDetalle, List<GrupoExcel<K>> grupoDetalle)
        {
            ExcelPackage package = new ExcelPackage();
            SetWorkbookProperties(ref package);

            int numeroHoja = 0;

            ExcelWorksheet workSheet = CreateSheet(package, "RESUMEN", numeroHoja++);

            int rowIndex = rowEncab;
            int colIndex = 2;

            foreach (var grupo in grupoDetalle)
            {
                colIndex = 2;

                if (grupo.ConCabecera)
                {
                    columnasDetalle.First().Nombre = $"{grupo.Nombre}";
                    CreateHeader(ref workSheet, ref rowIndex, columnasDetalle);
                }

                foreach (var tabla in grupo.Tablas)
                {
                    colIndex = 2;
                    if (tabla.DataHeader.IsNotNull())
                        CreateData(ref workSheet, ref rowIndex, tabla.DataHeader, columnasDetalle, ref colIndex, Color.FromArgb(191, 191, 191));
                    colIndex = 2;
                    if (tabla.Data.Count > 0)
                    {
                        CreateData(ref workSheet, ref rowIndex, tabla.Data, columnasDetalle, ref colIndex);
                        colIndex = 2;
                    }

                    if (tabla.DataFooter.IsNotNull())
                        CreateData(ref workSheet, ref rowIndex, tabla.DataFooter, columnasDetalle, ref colIndex, Color.FromArgb(191, 191, 191));
                    ++rowIndex;
                }
                colIndex = 2;

                if (grupo.DataFooter.IsNotNull())
                {
                    CreateData(ref workSheet, ref rowIndex, grupo.DataFooter, columnasDetalle, ref colIndex, Color.FromArgb(191, 191, 191));
                }
                ++rowIndex;
                ++rowIndex;
            }

            CreateTitle(ref workSheet, colIndex, hojas[0]);
            CreateFooter(ref workSheet, colIndex);

            foreach (var hoja in hojas)
            {
                workSheet = CreateSheet(package, hoja.Nombre, numeroHoja++);
                rowIndex = rowEncab;
                colIndex = 2;

                foreach (var columnas in hoja.Columnas)
                {
                    CreateHeader(ref workSheet, ref rowIndex, columnas);
                }

                var maxData = hoja.Columnas.Count - 1;
                CreateData(ref workSheet, ref rowIndex, hoja.Data, hoja.Columnas[maxData], ref colIndex);
                CreateTitle(ref workSheet, colIndex, hoja);
                CreateFooter(ref workSheet, colIndex);
            }

            byte[] bytes = package.GetAsByteArray();

            return Convert.ToBase64String(bytes, 0, bytes.Length);
        }

    }

    public class HojaExcel<T>
    {
        public string Titulo { get; set; }
        public string Negocio { get; set; }
        public DateTime? Desde { get; set; }
        public DateTime? FechaConsulta { get; set; }
        public DateTime? Hasta { get; set; }
        public string Nombre { get; set; }
        public List<T> Data { get; set; }
        public InsertRow AgregaFila { get; set; }
        public List<List<ColumnaExcel>> Columnas { get; set; }
        public int? FilaInmobilizacion { get; set; }
    }

    public class HojaExcelDataTable
    {
        public string Titulo { get; set; }
        public string Negocio { get; set; }
        public DateTime? Desde { get; set; }
        public DateTime? FechaConsulta { get; set; }
        public DateTime? Hasta { get; set; }
        public string Nombre { get; set; }
        public DataTable Data { get; set; }
        public InsertRow AgregaFila { get; set; }
        public List<List<ColumnaExcel>> Columnas { get; set; }
    }

    public class HojaExcelDistribucionfondo
    {
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public DateTime FechaIncioVgn { get; set; }
        public DateTime? FechaFinVgn { get; set; }
        public string Estado { get; set; }
        public DataTable Data { get; set; }
        public InsertRow AgregaFila { get; set; }
        public List<List<ColumnaExcel>> Columnas { get; set; }
    }

    public class HojaExcelAllDistribucionfondo
    {
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public DataTable Data { get; set; }
        public InsertRow AgregaFila { get; set; }
        public List<List<ColumnaExcel>> Columnas { get; set; }
    }

    public class ColumnaExcel
    {
        public string KeyColumna { get; set; }
        public string Nombre { get; set; }
        public FormatoExcel Formato { get; set; }
        public int? NroDecimales { get; set; }
    }

    public class InsertRow
    {
        public int CadaCuanto { get; set; }
        public int LineasAgregar { get; set; }
    }

    public class GrupoExcel<T>
    {
        public string Nombre { get; set; }
        public bool ConCabecera { get; set; }
        public List<TablaExcel<T>> Tablas { get; set; }
        public List<T> DataFooter { get; set; }
    }

    public class TablaExcel<T>
    {
        public List<T> DataHeader { get; set; }
        public List<T> DataFooter { get; set; }
        public List<T> Data { get; set; }
    }

    public enum FormatoExcel
    {
        Oculto,
        Texto,
        Porcentaje,
        Moneda,
        Fecha,
        FechaConHora,
        Numero,
        Merge,
        Hora,
        General,
        AplicaMerge
    }

    public static class Extensores
    {
        public static Boolean IsNotNull(this Object objeto)
        {
            return objeto != null;
        }

        public static DataTable ToPivotTable<T, TColumn, TRow, TData>(
            this IEnumerable<T> source,
            Func<T, TColumn> columnSelector,
            Expression<Func<T, TRow>> rowSelector,
            Func<IEnumerable<T>, TData> dataSelector)
        {
            DataTable table = new DataTable();
            var rowName = ((MemberExpression)rowSelector.Body).Member.Name;
            table.Columns.Add(new DataColumn(rowName, typeof(DateTime)));
            var columns = source.Select(columnSelector).Distinct();

            foreach (var column in columns)
                table.Columns.Add(new DataColumn(column.ToString(), typeof(decimal)));

            var rows = source.GroupBy(rowSelector.Compile())
                             .Select(rowGroup => new
                             {
                                 rowGroup.Key,
                                 Values = columns.GroupJoin(
                                     rowGroup,
                                     c => c,
                                     r => columnSelector(r),
                                     (c, columnGroup) => dataSelector(columnGroup))
                             });

            foreach (var row in rows)
            {
                var dataRow = table.NewRow();
                var items = row.Values.Cast<object>().ToList();
                items.Insert(0, row.Key);
                dataRow.ItemArray = items.ToArray();
                table.Rows.Add(dataRow);
            }

            return table;
        }

        public static List<dynamic> ToDynamicList(this DataTable dt)
        {
            var list = new List<dynamic>();
            foreach (DataRow row in dt.Rows)
            {
                dynamic dyn = new ExpandoObject();
                list.Add(dyn);
                foreach (DataColumn column in dt.Columns)
                {
                    var dic = (IDictionary<string, object>)dyn;
                    dic[column.ColumnName] = row[column];
                }
            }
            return list;
        }
    }

}
